// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-11-27 16:19:25

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

namespace Harmony
{
    public static partial class Tags
    {
        public const string Terrain = "Terrain"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string Fauna = "Fauna"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string GameController = "GameController"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string Player = "Player"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string UICanvas = "UICanvas"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string HomeController = "HomeController"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string DeadMenu = "DeadMenu"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string NavigationMesh = "NavigationMesh"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string ObjectPool = "ObjectPool"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string Ambient = "Ambient"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string Water = "Water"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string Flora = "Flora"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string LevelController = "LevelController"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string Untagged = "Untagged"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string EditorOnly = "EditorOnly"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string PauseMenu = "PauseMenu"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string Obstacle = "Obstacle"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string Music = "Music"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string Finish = "Finish"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string MainController = "MainController"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string GameCamera = "GameCamera"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string Respawn = "Respawn"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public const string MainCamera = "MainCamera"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
    }
}