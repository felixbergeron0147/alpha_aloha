// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-11-27 16:19:25

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

namespace Harmony
{
    public static partial class Scenes
    {
        public const string test_player_enemy_collision = "test_player_enemy_collision"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_enemy_collision.unity".
        public const string Main = "Main"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Main.unity".
        public const string End = "End"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\End.unity".
        public const string Home = "Home"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string test_player_color_change = "test_player_color_change"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string LevelFire = "LevelFire"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string TestCameraZoomOut = "TestCameraZoomOut"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\TestCameraZoomOut.unity".
        public const string Game = "Game"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string test_FGCliche = "test_FGCliche"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_FGCliche.unity".
        public const string LevelIce = "LevelIce"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string PauseMenu = "PauseMenu"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string Hub = "Hub"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string Level2 = "Level2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string DeadMenu = "DeadMenu"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\DeadMenu.unity".
        public const string Level3 = "Level3"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level3.unity".
        public const string Tutorial = "Tutorial"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string LoadingScreen = "LoadingScreen"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\LoadingScreen.unity".
        // "demo scene" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\demo scene.unity".
        public const string Level1 = "Level1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
    }
}