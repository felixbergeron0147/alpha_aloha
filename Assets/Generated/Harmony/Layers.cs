// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-11-27 16:19:25

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Layers
    {
        // "Ignore Raycast" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public static readonly LayerMask Water = LayerMask.NameToLayer("Water"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public static readonly LayerMask Background = LayerMask.NameToLayer("Background"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public static readonly LayerMask TransparentFX = LayerMask.NameToLayer("TransparentFX"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public static readonly LayerMask Sensor = LayerMask.NameToLayer("Sensor"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public static readonly LayerMask UI = LayerMask.NameToLayer("UI"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public static readonly LayerMask Default = LayerMask.NameToLayer("Default"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public static readonly LayerMask Terrain = LayerMask.NameToLayer("Terrain"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
        public static readonly LayerMask Player = LayerMask.NameToLayer("Player"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\ProjectSettings\TagManager.asset".
    }
}