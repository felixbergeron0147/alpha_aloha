// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-11-27 16:19:25

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class AnimatorParameters
    {
        public static readonly int IsFalling = Animator.StringToHash("IsFalling"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\PlayerMotionAnimator.controller".
        public static readonly int Openning = Animator.StringToHash("Openning"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Lava Cave\png\Animated Objects\Crate\Crate (2).controller".
        public static readonly int JumpStart = Animator.StringToHash("JumpStart"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\JumpEffectAnimator.controller".
        public static readonly int StartDie = Animator.StringToHash("StartDie"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\PlayerMotionAnimator.controller".
        public static readonly int IsRunning = Animator.StringToHash("IsRunning"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\PlayerMotionAnimator.controller".
        public static readonly int StartDash = Animator.StringToHash("StartDash"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\PlayerMotionAnimator.controller".
        public static readonly int IsJumping = Animator.StringToHash("IsJumping"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\PlayerMotionAnimator.controller".
        public static readonly int isTriggered = Animator.StringToHash("isTriggered"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Sprites\Geyser\geyserController.controller".
        public static readonly int DashStart = Animator.StringToHash("DashStart"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\DashEffectAnimator.controller".
        public static readonly int ColorBlend = Animator.StringToHash("ColorBlend"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\PlayerMotionAnimator.controller".
        public static readonly int FreezeStart = Animator.StringToHash("FreezeStart"); // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\FreezeEffectAnimator.controller".
    }
}