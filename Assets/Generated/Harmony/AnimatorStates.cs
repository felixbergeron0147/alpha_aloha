// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-11-27 16:19:25

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

namespace Harmony
{
    public static partial class AnimatorStates
    {
        public const string RB = "RB"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\GameObject.controller".
        public const string LT = "LT"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\LT.controller".
        // "Thumbstick 360rot" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Thumbstick 360rotation.controller".
        public const string LB = "LB"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\LB.controller".
        public const string Jumping = "Jumping"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\PlayerMotionAnimator.controller".
        // "Thimbstick Up and Down" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Thumbstick Up 1.controller".
        public const string RT = "RT"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\RT.controller".
        // "Thumbstick Down" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Thumbstick Down.controller".
        // "New State" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Sprites\Geyser\geyserController.controller".
        // "New State 0" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Sprites\Geyser\geyserController.controller".
        public const string Idle = "Idle"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\PlayerMotionAnimator.controller".
        // "Thumbstick Down-right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Thumbstick Down-right.controller".
        // "Thumbstick Up" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Thumbstick Up.controller".
        // "D-Pad Right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\D-pad Right.controller".
        // "D-Pad Left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\D-pad Left.controller".
        // "X button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\X button.controller".
        // "Back 0" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Back.controller".
        // "Thumbstick Up-right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Thumbstick Up-right.controller".
        public const string FreezeEffect = "FreezeEffect"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\FreezeEffectAnimator.controller".
        public const string Dying = "Dying"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\PlayerMotionAnimator.controller".
        public const string Dashing = "Dashing"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\DashEffectAnimator.controller".
        // "D-Pad Down" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\D-pad Up 1.controller".
        public const string Start = "Start"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Start.controller".
        // "D-Pad Up" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\D-pad Up.controller".
        // "Thumbstick Right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Thumbstick Right.controller".
        public const string Fireball = "Fireball"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Lava Cave\animations\Fireball Large (1).controller".
        public const string Falling = "Falling"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\PlayerMotionAnimator.controller".
        public const string Running = "Running"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\PlayerMotionAnimator.controller".
        public const string geyserAnimation = "geyserAnimation"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Sprites\Geyser\geyserController.controller".
        public const string Default = "Default"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Lava Cave\png\Animated Objects\Crate\Crate (2).controller".
        // "Thumbstick Left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Thumbstick Left.controller".
        // "A button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\A button.controller".
        public const string Torch = "Torch"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Lava Cave\png\Animated Objects\Torch\Torch2.controller".
        public const string Jump = "Jump"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Animators\JumpEffectAnimator.controller".
        // "Thumbstick Down-left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Thumbstick Down-left.controller".
        // "Y button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Y1.controller".
        public const string Back = "Back"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Back.controller".
        // "Thumbstick Left and right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Thumbstick Left and Right.controller".
        // "B button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\B button.controller".
        public const string FireballSmall = "FireballSmall"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Lava Cave\animations\Fireball Small (4).controller".
        // "Thumbstick Up-left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\Thumbstick Up-left.controller".
        public const string Openning = "Openning"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Lava Cave\png\Animated Objects\Crate\Crate (2).controller".
    }
}