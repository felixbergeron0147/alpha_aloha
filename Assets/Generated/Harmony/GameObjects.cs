// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-11-27 16:19:25

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

namespace Harmony
{
    public static partial class GameObjects
    {
        public const string HotZone2 = "HotZone2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string Movement = "Movement"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string StaticSigns = "StaticSigns"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string ef_12_normal = "ef_12_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_normal.prefab".
        public const string ef_15_blue = "ef_15_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_blue.prefab".
        public const string Rocket_blue = "Rocket_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_blue.prefab".
        public const string FreezeWater = "FreezeWater"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string Flame_3 = "Flame_3"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_3.prefab".
        public const string PlayerLifeBar = "PlayerLifeBar"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "ThinDestroyableVase (3)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string splash = "splash"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\splash.prefab".
        public const string ef_4_normal = "ef_4_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_normal.prefab".
        public const string Trail_blue = "Trail_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_blue.prefab".
        public const string Sign = "Sign"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string Viewport = "Viewport"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string LvlBoundary0 = "LvlBoundary0"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string burn_effect = "burn_effect"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\burn_effect.prefab".
        public const string Character = "Character"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Character.prefab".
        public const string ef_18_purple = "ef_18_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_purple.prefab".
        public const string ef_1_normal = "ef_1_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_1_normal.prefab".
        public const string StartButton = "StartButton"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string ButtonSpriteRenderer = "ButtonSpriteRenderer"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string TemporaryPlatform = "TemporaryPlatform"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Tiles\TilePalettes\TemporaryPlatform.prefab".
        public const string ResolutonTitle = "ResolutonTitle"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "Item Label" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Load = "Load"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string CheckBox = "CheckBox"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string GrapplePoint = "GrapplePoint"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\GrapplePoint.prefab".
        public const string ef_17_purple = "ef_17_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_purple.prefab".
        public const string Trail_green = "Trail_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_green.prefab".
        public const string Torches = "Torches"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string Door2 = "Door2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "DashDestroyableCrystal (5)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        // "TrapDoor1.2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Destruction_air_blue = "Destruction_air_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_blue.prefab".
        public const string ef_3_normal = "ef_3_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_normal.prefab".
        public const string smoke_4 = "smoke_4"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_4.prefab".
        public const string Muzzle_dron_normal = "Muzzle_dron_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_normal.prefab".
        public const string Fill = "Fill"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string LoadingAnimation = "LoadingAnimation"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\LoadingScreen.unity".
        public const string Label = "Label"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string TrapDoor = "TrapDoor"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string Flame_4 = "Flame_4"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_4.prefab".
        public const string ef_13_green = "ef_13_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_green.prefab".
        public const string Mine_green = "Mine_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_green.prefab".
        public const string DeadText = "DeadText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string ef_20_green = "ef_20_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_green.prefab".
        public const string ef_23_green = "ef_23_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_green.prefab".
        // "Item Checkmark" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "Torch1.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Lazer_normal = "Lazer_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_normal.prefab".
        public const string LifeBarFillSprite = "LifeBarFillSprite"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string expl_01_05 = "expl_01_05"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_05.prefab".
        // "TrapDoor (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string ef_23_blue = "ef_23_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_blue.prefab".
        // "DashDestroyableCrystal (3)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string TemporaryPlateform = "TemporaryPlateform"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        // "Torch " is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string Muzzle_shotgun_green = "Muzzle_shotgun_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_green.prefab".
        public const string ef_17_normal = "ef_17_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_normal.prefab".
        public const string Ground_explosion_green = "Ground_explosion_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_green.prefab".
        public const string ef_18_green = "ef_18_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_green.prefab".
        public const string Lava = "Lava"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string ef_3_green = "ef_3_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_green.prefab".
        public const string Mine_purple = "Mine_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_purple.prefab".
        public const string YButton = "YButton"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        // "D-pad Up" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\D-pad Up.prefab".
        public const string ef_13_purple = "ef_13_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_purple.prefab".
        public const string ef_13_blue = "ef_13_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_blue.prefab".
        public const string Medieval_props_free_9 = "Medieval_props_free_9"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_9.prefab".
        public const string ConfirmDelete = "ConfirmDelete"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Arrow = "Arrow"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string CeilingAndWall = "CeilingAndWall"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string Medieval_props_free_21 = "Medieval_props_free_21"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_21.prefab".
        public const string TrapDoor5 = "TrapDoor5"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Flame_6 = "Flame_6"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_6.prefab".
        public const string Explosion_1_01 = "Explosion_1_01"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_01.prefab".
        public const string ef_22_blue = "ef_22_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_blue.prefab".
        // "Thumbstick Left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Left.prefab".
        public const string SignDangers = "SignDangers"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string ArrowSign = "ArrowSign"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string smoke_5 = "smoke_5"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_5.prefab".
        // "MeltableIceBlock (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string BackgroundRight = "BackgroundRight"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Start = "Start"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Start.prefab".
        public const string BackButton = "BackButton"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string SaveSpaceChoice3 = "SaveSpaceChoice3"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "Crate4.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "ArrowSign3.2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Medieval_props_free_10 = "Medieval_props_free_10"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_10.prefab".
        // "Torch4.2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Decorative2 = "Decorative2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string VerticalLayout = "VerticalLayout"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string XButton = "XButton"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string PlayerEffect = "PlayerEffect"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Player.prefab".
        public const string JumpEffectAnimator = "JumpEffectAnimator"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Player.prefab".
        public const string Muzzle_tank_green = "Muzzle_tank_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_green.prefab".
        public const string MenuBackgroundGroup = "MenuBackgroundGroup"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "Virtual Camera" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        // "Main Camera" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Controller sheets\demo scene.unity".
        // "Lava (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        // "WanderingEnemy (4)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string Quit = "Quit"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\End.unity".
        public const string ef_16_blue = "ef_16_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_blue.prefab".
        public const string Destruction_air_green = "Destruction_air_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_green.prefab".
        public const string SaveSpaceChoice = "SaveSpaceChoice"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\SaveSpaceChoice.prefab".
        public const string FreezeTriggerZone = "FreezeTriggerZone"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Player.prefab".
        public const string MovingPlatform = "MovingPlatform"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Receivers\MovingPlatformGroup.prefab".
        public const string CancelButton = "CancelButton"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Destruction_air_normal = "Destruction_air_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_normal.prefab".
        public const string Muzzle_tank_purple = "Muzzle_tank_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_purple.prefab".
        public const string HomeController = "HomeController"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Dash = "Dash"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string FallingEnemy = "FallingEnemy"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_enemy_collision.unity".
        public const string Player = "Player"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\TestCameraZoomOut.unity".
        public const string ef_15_green = "ef_15_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_green.prefab".
        public const string Vitals = "Vitals"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\TestCameraZoomOut.unity".
        // "TemporaryPlatform (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        // "TemporaryPlatform (6)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string Interactibles = "Interactibles"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        // "TrapDoor (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string ef_8_2_purple = "ef_8_2_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_purple.prefab".
        // "TemporaryPlatform (9)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string ef_14_purple = "ef_14_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_purple.prefab".
        public const string NewGameMenu = "NewGameMenu"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string ChangeState = "ChangeState"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string Medieval_props_free_8 = "Medieval_props_free_8"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_8.prefab".
        public const string Button = "Button"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string SignFreezeWater = "SignFreezeWater"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string Checkmark = "Checkmark"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string ef_23_normal = "ef_23_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_normal.prefab".
        // "D-pad Right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\D-pad Right.prefab".
        // "MovingPlatformGroup4.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "Torch2.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ground_explosion = "ground_explosion"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\ground_explosion.prefab".
        // "TemporaryPlatform (7)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string Move = "Move"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string SignChangeTemperature = "SignChangeTemperature"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string EndMessage = "EndMessage"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\End.unity".
        public const string Mover = "Mover"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\TestCameraZoomOut.unity".
        public const string RTTrigger = "RTTrigger"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string PlayerCamera = "PlayerCamera"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Camera\LevelCameras.prefab".
        public const string ef_8_1_green = "ef_8_1_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_green.prefab".
        public const string Particles = "Particles"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_1.prefab".
        public const string Ground_explosion_purple = "Ground_explosion_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_purple.prefab".
        public const string Background = "Background"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\End.unity".
        public const string BackGround2 = "BackGround2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string SpawnHub = "SpawnHub"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\SpawnPoint\SpawnHub.prefab".
        public const string Timer = "Timer"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string VolumeSlider = "VolumeSlider"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Grapple = "Grapple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Player.prefab".
        public const string expl_02_03 = "expl_02_03"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_03.prefab".
        public const string expl_02_04 = "expl_02_04"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_04.prefab".
        public const string TemperatureBarBackground = "TemperatureBarBackground"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\TemperatureBarVisual.prefab".
        public const string ef_18_normal = "ef_18_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_normal.prefab".
        public const string Bars = "Bars"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string Rocket_purple = "Rocket_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_purple.prefab".
        public const string ef_3_blue = "ef_3_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_blue.prefab".
        // "WanderingEnemy4.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "ArrowSign4.3" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "WanderingEnemy2.3" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "ArrowSign 1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        // "DeadSign (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string DashDestroyableCrystal = "DashDestroyableCrystal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string Rocket_normal = "Rocket_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_normal.prefab".
        public const string ef_14_blue = "ef_14_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_blue.prefab".
        public const string ef_4_purple = "ef_4_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_purple.prefab".
        public const string LvlBoundaries = "LvlBoundaries"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string Freeze = "Freeze"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        // "Door (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string ef_13_normal = "ef_13_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_normal.prefab".
        public const string Lazer_purple = "Lazer_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_purple.prefab".
        public const string Image = "Image"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "Geyser (4)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string SaveSpaceChoice1 = "SaveSpaceChoice1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string ef_8_2_normal = "ef_8_2_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_normal.prefab".
        public const string FirstPlanDecoratives = "FirstPlanDecoratives"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string ef_2_normal = "ef_2_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_2_normal.prefab".
        public const string expl_01_02 = "expl_01_02"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_02.prefab".
        public const string AbilityCooldown = "AbilityCooldown"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\AbilityCooldown.prefab".
        // "ArrowSign (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string FrozenStateStructure = "FrozenStateStructure"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\TemperatureManagable\FreezableWaterSource.prefab".
        // "Geyser (5)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string AchievementCanvas = "AchievementCanvas"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string Save1 = "Save1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string sign = "sign"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\InformationSign.prefab".
        public const string SlideText = "SlideText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string GameObject = "GameObject"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "Button (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string ef_25_blue = "ef_25_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_blue.prefab".
        public const string ef_8_1_normal = "ef_8_1_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_normal.prefab".
        public const string RetryGame = "RetryGame"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string ef_12_purple = "ef_12_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_purple.prefab".
        public const string OptionsMenu = "OptionsMenu"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "Thumbstick Up-left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Up-left.prefab".
        public const string RBButton = "RBButton"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        // "FatDestroyableVase (3)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string EssentialRespawner = "EssentialRespawner"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Crate.prefab".
        public const string Template = "Template"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string LvlBoundary2 = "LvlBoundary2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string LevelTilemap = "LevelTilemap"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string SaveGame = "SaveGame"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\SaveGame.prefab".
        public const string ef_24_green = "ef_24_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_green.prefab".
        public const string ef_17_green = "ef_17_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_green.prefab".
        public const string TextControls = "TextControls"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        // "Door4.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string explosion_2 = "explosion_2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_2.prefab".
        // "WanderingEnemy3.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string MeltIce = "MeltIce"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        // "ArrowSign4.4" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ef_18_blue = "ef_18_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_blue.prefab".
        public const string Name = "Name"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\End.unity".
        public const string FullscreenCheck = "FullscreenCheck"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string InteractText = "InteractText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string NoSaveGame = "NoSaveGame"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\NoSaveGame.prefab".
        public const string ef_23_purple = "ef_23_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_purple.prefab".
        public const string HorizontalLayoutGroup = "HorizontalLayoutGroup"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        // "JumpingEnemy4.2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Trail_purple = "Trail_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_purple.prefab".
        // "Torch (5)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string ef_19_normal = "ef_19_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_normal.prefab".
        public const string TriggerSensor = "TriggerSensor"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\TestCameraZoomOut.unity".
        public const string NewGame = "NewGame"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Canvas = "Canvas"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\LoadingScreen.unity".
        public const string ef_21_normal = "ef_21_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_normal.prefab".
        public const string Ground_explosion_blue = "Ground_explosion_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_blue.prefab".
        // "ArrowSign4.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string FreezeSign = "FreezeSign"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        // "Sign (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        // "TemporaryPlatform (8)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string smoke_3 = "smoke_3"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_3.prefab".
        public const string LoadingText = "LoadingText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\LoadingScreen.unity".
        // "ArrowSign3.3" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "Thumbstick Up 1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Up 1.prefab".
        public const string Stimuli = "Stimuli"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Character.prefab".
        public const string AchievementContainer = "AchievementContainer"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string GameObjectGroup2 = "GameObjectGroup2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string frozen_enemy = "frozen_enemy"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string TemperatureBarVisual = "TemperatureBarVisual"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string Dangers = "Dangers"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        // "Torch2.4" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ResolutionDropDown = "ResolutionDropDown"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Spike = "Spike"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string DeadSign = "DeadSign"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string LeverSpriteRenderer = "LeverSpriteRenderer"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string ef_24_purple = "ef_24_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_purple.prefab".
        public const string ef_20_blue = "ef_20_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_blue.prefab".
        public const string ef_12_blue = "ef_12_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_blue.prefab".
        public const string Title = "Title"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "Torch1.3" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ricochet = "ricochet"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\ricochet.prefab".
        // "ThinDestroyableVase (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        // "JumpingEnemy (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string TemperatureWarning = "TemperatureWarning"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string Flame_2 = "Flame_2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_2.prefab".
        public const string LifeBarFill = "LifeBarFill"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "TrapDoor1.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string launcher = "launcher"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\launcher.prefab".
        public const string PressurePlate = "PressurePlate"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string TemperatureBarFillSprite = "TemperatureBarFillSprite"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "Thumbstick Up-right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Up-right.prefab".
        public const string GeyserInformation = "GeyserInformation"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        // "Door (3)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string DoorFire = "DoorFire"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string expl_02_05 = "expl_02_05"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_05.prefab".
        public const string Sprite = "Sprite"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\TestCameraZoomOut.unity".
        public const string ef_16_purple = "ef_16_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_purple.prefab".
        // "ThinDestroyableVase (4)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string SignMovement = "SignMovement"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string MainMenu = "MainMenu"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "FreezableWaterSource4.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string SaveGameTime = "SaveGameTime"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\SaveGame.prefab".
        public const string Back = "Back"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Back.prefab".
        public const string Enemy = "Enemy"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Enemies\Enemy.prefab".
        public const string ef_11_purple = "ef_11_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_purple.prefab".
        // "Thumbstick Down" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Down.prefab".
        public const string doorEnd = "doorEnd"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\LoadTrigger\doorEnd.prefab".
        // "ArrowSign (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string Content = "Content"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "TemporaryPlatform (4)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string Enemies = "Enemies"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        // "DashDestroyableCrystal(2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string Delete = "Delete"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\SaveGame.prefab".
        // "B button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\B button.prefab".
        public const string ef_20_normal = "ef_20_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_normal.prefab".
        // "AbilityCooldown(Freeze)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string Water = "Water"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string Wall = "Wall"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Level\LevelTilemap.prefab".
        public const string ArrowTrap = "ArrowTrap"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string Loader = "Loader"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\LoadTrigger\Loader.prefab".
        public const string ef_7_normal = "ef_7_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_normal.prefab".
        public const string Terrain = "Terrain"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\TestCameraZoomOut.unity".
        public const string BackgroundLeft = "BackgroundLeft"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string ColdZone = "ColdZone"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string LiquidStateStructure = "LiquidStateStructure"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\TemperatureManagable\FreezableWaterSource.prefab".
        // "Geyser (6)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string Scrollbar = "Scrollbar"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string RT = "RT"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\RT.prefab".
        public const string ThinDestroyableVase = "ThinDestroyableVase"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string SaveName = "SaveName"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\SaveGame.prefab".
        public const string ef_6_blue = "ef_6_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_blue.prefab".
        public const string EnemyIceBlockController = "EnemyIceBlockController"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Enemies\Enemy.prefab".
        public const string ef_21_blue = "ef_21_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_blue.prefab".
        // "ArrowSign (3)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string MiddleAnchor = "MiddleAnchor"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\SaveGame.prefab".
        public const string smoke_1 = "smoke_1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_1.prefab".
        public const string Flame_5 = "Flame_5"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_5.prefab".
        public const string Achievement = "Achievement"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "Lever4.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "DeadSign (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string DashBreakInformation = "DashBreakInformation"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string ef_25_normal = "ef_25_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_normal.prefab".
        public const string PauseMenu = "PauseMenu"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string LvlBoundary3 = "LvlBoundary3"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        // "PressurePlate4.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string explosion_5 = "explosion_5"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_5.prefab".
        // "Lava (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string ef_4_blue = "ef_4_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_blue.prefab".
        public const string JumpingEnemy = "JumpingEnemy"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        // "ThinDestroyableVase (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        // "Thumbstick Down-right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Down-right.prefab".
        public const string Layer1 = "Layer1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Tiles\TilePalettes\TemporaryPlatform.prefab".
        public const string Muzzle_dron_green = "Muzzle_dron_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_green.prefab".
        public const string DoubleJumpInput = "DoubleJumpInput"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "Crate1.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string SaveDate = "SaveDate"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\SaveGame.prefab".
        public const string LevelDoors = "LevelDoors"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string VerticalLayoutGroup = "VerticalLayoutGroup"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\End.unity".
        // "WanderingEnemy3.2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "Torch2.6" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Anchor = "Anchor"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string ef_14_green = "ef_14_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_green.prefab".
        public const string Muzzle_shotgun_normal = "Muzzle_shotgun_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_normal.prefab".
        public const string DeadMenu = "DeadMenu"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "Torch2.5" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string smoke_2 = "smoke_2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_2.prefab".
        public const string TutorialSigns = "TutorialSigns"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string PlayerTemperatureManager = "PlayerTemperatureManager"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string electricity = "electricity"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\electricity.prefab".
        public const string ChangeStateText = "ChangeStateText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string explosion_1 = "explosion_1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_1.prefab".
        public const string Door1 = "Door1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ef_7_purple = "ef_7_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_purple.prefab".
        public const string ef_9_normal = "ef_9_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_normal.prefab".
        // "MeltableIceBlock (3)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string StartSpawnpoint = "StartSpawnpoint"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string TrapDoor3 = "TrapDoor3"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ef_10_normal = "ef_10_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_normal.prefab".
        public const string Mine_normal = "Mine_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_normal.prefab".
        public const string Torch = "Torch"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        // "TemporaryPlatform (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        // "ArrowSign4.2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ef_16_green = "ef_16_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_green.prefab".
        // "MovingPlatformNode (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Receivers\MovingPlatformGroup.prefab".
        // "Thumbstick Down-left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Down-left.prefab".
        // "AbilityCooldown(Dash)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string ef_10_purple = "ef_10_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_purple.prefab".
        public const string fire = "fire"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\fire.prefab".
        public const string QuitGame = "QuitGame"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string ef_24_blue = "ef_24_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_blue.prefab".
        public const string ef_17_blue = "ef_17_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_blue.prefab".
        public const string LvlBoundary1 = "LvlBoundary1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string Save3 = "Save3"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string TrapDoor4 = "TrapDoor4"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "DashDestroyableCrystal (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string Flame_1 = "Flame_1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_1.prefab".
        public const string Crate = "Crate"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        // "Item Background" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Medieval_props_free_1 = "Medieval_props_free_1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_1.prefab".
        public const string Glow = "Glow"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_1.prefab".
        public const string GameObjectGroup1 = "GameObjectGroup1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string HowToUseArtefact = "HowToUseArtefact"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string Rocket_green = "Rocket_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_green.prefab".
        public const string SaveDataAnchor = "SaveDataAnchor"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "Torch (4)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string SaveNameText = "SaveNameText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\SaveSpaceChoice.prefab".
        // "ArrowSign3.3 (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ef_22_purple = "ef_22_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_purple.prefab".
        public const string RB = "RB"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\RB.prefab".
        public const string FullLevelCamera = "FullLevelCamera"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string Muzzle_dron_purple = "Muzzle_dron_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_purple.prefab".
        public const string HotZone = "HotZone"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string TrapDoor1 = "TrapDoor1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string GameObjectGroup3 = "GameObjectGroup3"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string ef_11_normal = "ef_11_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_normal.prefab".
        public const string ef_15_normal = "ef_15_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_normal.prefab".
        // "Torch4.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ef_8_1_purple = "ef_8_1_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_purple.prefab".
        public const string Credits = "Credits"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\End.unity".
        public const string SpikeSpriteRenderer = "SpikeSpriteRenderer"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        // "New Sprite" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_FGCliche.unity".
        public const string GameController = "GameController"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "Torch3.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string BottomAnchor = "BottomAnchor"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\SaveGame.prefab".
        // "WanderingEnemy (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string ef_6_purple = "ef_6_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_purple.prefab".
        public const string Lever = "Lever"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        // "Torch2.3" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ef_15_purple = "ef_15_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_purple.prefab".
        // "PressurePlate1.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string expl_02_01 = "expl_02_01"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_01.prefab".
        public const string ef_11_green = "ef_11_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_green.prefab".
        public const string WanderingEnemy = "WanderingEnemy"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_enemy_collision.unity".
        public const string Pause = "Pause"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string UICanvas = "UICanvas"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "Torch3.2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Muzzle_tank_normal = "Muzzle_tank_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_normal.prefab".
        public const string PlayerMotionAnimator = "PlayerMotionAnimator"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Player.prefab".
        public const string ef_8_1_blue = "ef_8_1_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_blue.prefab".
        // "Door1.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "ArrowTrap (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string health_up = "health_up"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\health_up.prefab".
        // "Lever2.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ScrollView = "ScrollView"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "TemporaryPlatform (10)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string fire_big = "fire_big"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\fire_big.prefab".
        public const string Muzzle_dron_blue = "Muzzle_dron_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_blue.prefab".
        public const string Aura = "Aura"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Player.prefab".
        public const string DashSensor = "DashSensor"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Player.prefab".
        public const string Save2 = "Save2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string ef_14_normal = "ef_14_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_normal.prefab".
        public const string ef_6_green = "ef_6_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_green.prefab".
        public const string ef_22_normal = "ef_22_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_normal.prefab".
        public const string Options = "Options"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "D-pad Down" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\D-pad Down.prefab".
        // "Lava (3)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        // "Lava1.2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string MenuBackgroundGroupCanvas = "MenuBackgroundGroupCanvas"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "FatDestroyableVase (4)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string DashEffectAnimator = "DashEffectAnimator"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Player.prefab".
        public const string Door = "Door"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string ef_9_blue = "ef_9_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_blue.prefab".
        public const string VolumeTitle = "VolumeTitle"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string SaveSpaceChoice2 = "SaveSpaceChoice2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string SignTemporaryPlateforms = "SignTemporaryPlateforms"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string JumpText = "JumpText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        // "GameObjects Section3" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "Torch (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string PauseText = "PauseText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        // "WanderingEnemy (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string Ricochet_purple = "Ricochet_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_purple.prefab".
        public const string ObjectPools = "ObjectPools"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Level\LevelController.prefab".
        public const string ef_20_purple = "ef_20_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_purple.prefab".
        public const string FreezeEffectAnimator = "FreezeEffectAnimator"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Player.prefab".
        public const string ef_9_purple = "ef_9_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_purple.prefab".
        // "WanderingEnemy3.3" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string TriggerSensor2D = "TriggerSensor2D"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ef_16_normal = "ef_16_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_normal.prefab".
        public const string ef_3_purple = "ef_3_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_purple.prefab".
        public const string expl_01_04 = "expl_01_04"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_04.prefab".
        public const string MoveText = "MoveText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        // "TrapDoor3.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string EnemySpriteRenderer = "EnemySpriteRenderer"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string TemperatureZone = "TemperatureZone"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string MeltableIceBlock = "MeltableIceBlock"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        // "Door (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string explosion_4 = "explosion_4"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_4.prefab".
        public const string ef_24_normal = "ef_24_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_normal.prefab".
        // "Lava1.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string InformationSign = "InformationSign"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string Item = "Item"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Clock = "Clock"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\SaveGame.prefab".
        // "Sliding Area" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string ChangeTemperature = "ChangeTemperature"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        // "TrapDoor1.3" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string LoadSaveMenu = "LoadSaveMenu"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string TempStatePlayer = "TempStatePlayer"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string CrossSign = "CrossSign"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string ef_22_green = "ef_22_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_green.prefab".
        // "Y button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Y button.prefab".
        // "Lever (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string BackGround = "BackGround"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string Slide = "Slide"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string MovingPlatformNode = "MovingPlatformNode"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Receivers\MovingPlatformGroup.prefab".
        // "Thumbstick 360rotation" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick 360rotation.prefab".
        public const string ConfirmText = "ConfirmText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Lazer_blue = "Lazer_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_blue.prefab".
        // "TemporaryPlatform (3)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        // "Thumbstick Left and Right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Left and Right.prefab".
        public const string ef_5_purple = "ef_5_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_purple.prefab".
        public const string DeadMenuCanvas = "DeadMenuCanvas"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string AchievementMenu = "AchievementMenu"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string ThumbstickIdle = "ThumbstickIdle"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string Ricochet_blue = "Ricochet_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_blue.prefab".
        // "Thumbstick Right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Right.prefab".
        public const string TempratureBarFill = "TempratureBarFill"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "Handle Slide Area" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Muzzle_shotgun_blue = "Muzzle_shotgun_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_blue.prefab".
        // "A button (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string SignDoubleJump = "SignDoubleJump"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string DecorativeElements = "DecorativeElements"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        // "Lava3.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string FreezableWaterSource = "FreezableWaterSource"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string flame_side = "flame_side"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\flame_side.prefab".
        public const string Lazer_green = "Lazer_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_green.prefab".
        public const string Floor = "Floor"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        // "ArrowSign3.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string DoorLevel1 = "DoorLevel1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string Ground_explosion_normal = "Ground_explosion_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_normal.prefab".
        public const string ef_25_green = "ef_25_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_green.prefab".
        public const string Grappling = "Grappling"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string FreezeText = "FreezeText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string BButton = "BButton"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string expl_01_03 = "expl_01_03"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_03.prefab".
        public const string Muzzle_tank_blue = "Muzzle_tank_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_blue.prefab".
        // "Geyser (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string Filler = "Filler"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        // "WanderingEnemy2.2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string smoke_side = "smoke_side"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_side.prefab".
        public const string LevelCameras = "LevelCameras"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\TestCameraZoomOut.unity".
        public const string cm = "cm"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        public const string Artefact = "Artefact"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "X button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\X button.prefab".
        // "GameObjects Section4" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string ef_5_blue = "ef_5_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_blue.prefab".
        public const string ef_5_green = "ef_5_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_green.prefab".
        // "FatDestroyableVase (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        // "Geyser (3)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string ef_19_green = "ef_19_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_green.prefab".
        public const string DecorativeObjects = "DecorativeObjects"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string ef_11_blue = "ef_11_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_blue.prefab".
        public const string ConfirmButton = "ConfirmButton"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        // "TemporaryPlatform (5)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string Geyser = "Geyser"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string TemperatureBarFill = "TemperatureBarFill"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        // "A button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\A button.prefab".
        public const string AbilitiesIcons = "AbilitiesIcons"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "ArrowSign1.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string character_idle_cold = "character_idle_cold"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string Decorative = "Decorative"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Level\LevelTilemap.prefab".
        public const string MovingPlatformPath = "MovingPlatformPath"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Receivers\MovingPlatformGroup.prefab".
        // "MovingPlatformGroup2.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Text = "Text"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string DoorLevel3 = "DoorLevel3"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string TemperatureBar = "TemperatureBar"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string ef_5_normal = "ef_5_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_normal.prefab".
        public const string Mine_blue = "Mine_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_blue.prefab".
        // "Torch2.2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string EventSystem = "EventSystem"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string ef_25_purple = "ef_25_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_purple.prefab".
        public const string ef_6_normal = "ef_6_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_normal.prefab".
        public const string MainCamera = "MainCamera"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        public const string Smoke_1 = "Smoke_1"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_01.prefab".
        public const string Shotgun_hit_normal = "Shotgun_hit_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_normal.prefab".
        public const string LB = "LB"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\LB.prefab".
        public const string Destruction_air_purple = "Destruction_air_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_purple.prefab".
        public const string MovingPlatformGroup = "MovingPlatformGroup"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelFire.unity".
        public const string ready_attack = "ready_attack"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\ready_attack.prefab".
        // "Lever3.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string expl_02_02 = "expl_02_02"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_02.prefab".
        public const string Ricochet_normal = "Ricochet_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_normal.prefab".
        public const string SignInteractibles = "SignInteractibles"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string Medieval_props_free_3 = "Medieval_props_free_3"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_3.prefab".
        public const string fire_small = "fire_small"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\fire_small.prefab".
        public const string Sparkles = "Sparkles"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_01.prefab".
        // "MeltableIceBlock (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string ef_19_blue = "ef_19_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_blue.prefab".
        public const string Tutorial = "Tutorial"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Tiles\TilePalettes\Tutorial.prefab".
        public const string Medieval_props_free_11 = "Medieval_props_free_11"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_11.prefab".
        public const string Medieval_props_free_5 = "Medieval_props_free_5"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_5.prefab".
        // "FreezableWaterSource (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string SignMeltIce = "SignMeltIce"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string plazma = "plazma"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\plazma.prefab".
        public const string ef_21_green = "ef_21_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_green.prefab".
        public const string Medieval_props_free_12 = "Medieval_props_free_12"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_12.prefab".
        public const string Trail_normal = "Trail_normal"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_normal.prefab".
        public const string BackgroundCanvas = "BackgroundCanvas"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "GameObjects Section2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "Thumbstick Up" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Up.prefab".
        public const string ef_4_green = "ef_4_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_green.prefab".
        public const string EndLevel = "EndLevel"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        // "FatDestroyableVase (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string ef_8_2_blue = "ef_8_2_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_blue.prefab".
        public const string HealthAndTemperature = "HealthAndTemperature"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "Torch1.2" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Medieval_props_free_2 = "Medieval_props_free_2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_2.prefab".
        public const string Medieval_props_free_4 = "Medieval_props_free_4"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_4.prefab".
        public const string LevelController = "LevelController"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_player_color_change.unity".
        // "WanderingEnemy2.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        // "WanderingEnemy (3)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level2.unity".
        public const string Shotgun_hit_green = "Shotgun_hit_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_green.prefab".
        public const string DecorativeBackground = "DecorativeBackground"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string ef_7_blue = "ef_7_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_blue.prefab".
        // "Torch (6)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string granade = "granade"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\granade.prefab".
        public const string SignTemperatureWarning = "SignTemperatureWarning"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string Handle = "Handle"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string GrapplingText = "GrapplingText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string VolumeValue = "VolumeValue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string ResumeGame = "ResumeGame"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string Smoke_2 = "Smoke_2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_01.prefab".
        public const string ef_9_green = "ef_9_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_green.prefab".
        // "Fill Area" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string DashText = "DashText"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        // "D-pad Left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\D-pad Left.prefab".
        public const string MainController = "MainController"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Main.unity".
        public const string LevelTemplate = "LevelTemplate"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Tests\test_FGCliche.unity".
        // "GameObjects Section1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string AButton = "AButton"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        // "JumpingEnemy3.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Muzzle_shotgun_purple = "Muzzle_shotgun_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_purple.prefab".
        public const string ef_10_green = "ef_10_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_green.prefab".
        // "Torch (2)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        // "Lever1.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string AchievementNotification = "AchievementNotification"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Game.unity".
        // "Torch (3)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string ef_19_purple = "ef_19_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_purple.prefab".
        public const string Shotgun_hit_blue = "Shotgun_hit_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_blue.prefab".
        public const string VolumeAnchor = "VolumeAnchor"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string ef_10_blue = "ef_10_blue"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_blue.prefab".
        // "JumpingEnemy4.1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Level1.unity".
        public const string Medieval_props_free_6 = "Medieval_props_free_6"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_6.prefab".
        // "Sign (1)" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Tutorial.unity".
        public const string Ricochet_green = "Ricochet_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_green.prefab".
        public const string FatDestroyableVase = "FatDestroyableVase"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\LevelIce.unity".
        public const string ef_8_2_green = "ef_8_2_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_green.prefab".
        public const string Jump = "Jump"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string ResolutionAnchor = "ResolutionAnchor"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string ef_12_green = "ef_12_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_green.prefab".
        public const string ef_7_green = "ef_7_green"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_green.prefab".
        public const string Shotgun_hit_purple = "Shotgun_hit_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_purple.prefab".
        public const string HorizontalLayout = "HorizontalLayout"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string End = "End"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\End.unity".
        public const string Description = "Description"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\Achievement.prefab".
        public const string LT = "LT"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\LT.prefab".
        public const string DoorLevel2 = "DoorLevel2"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string ef_21_purple = "ef_21_purple"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_purple.prefab".
        public const string BackgroundMiddle = "BackgroundMiddle"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\Home.unity".
        public const string Interact = "Interact"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Menus\PauseMenu.unity".
        public const string DoorIce = "DoorIce"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scenes\Levels\Hub.unity".
        public const string explosion_3 = "explosion_3"; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_3.prefab".
    }
}