// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-11-27 16:19:25

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public partial class Prefabs : MonoBehaviour
    {
        private static Prefabs instance;
    
        [SerializeField] private GameObject ef_20_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_green.prefab".
        [SerializeField] private GameObject ef_13_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_green.prefab".
        [SerializeField] private GameObject WanderingEnemyPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Enemies\WanderingEnemy.prefab".
        [SerializeField] private GameObject LeverPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Senders\Lever.prefab".
        [SerializeField] private GameObject ef_11_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_blue.prefab".
        [SerializeField] private GameObject PlayerPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Player.prefab".
        [SerializeField] private GameObject RBPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\RB.prefab".
        [SerializeField] private GameObject Medieval_props_free_21Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_21.prefab".
        [SerializeField] private GameObject ef_23_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_green.prefab".
        [SerializeField] private GameObject Flame_2Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_2.prefab".
        [SerializeField] private GameObject expl_01_01Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_01.prefab".
        [SerializeField] private GameObject burn_effectPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\burn_effect.prefab".
        [SerializeField] private GameObject ef_18_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_blue.prefab".
        [SerializeField] private GameObject LBPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\LB.prefab".
        [SerializeField] private GameObject ricochetPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\ricochet.prefab".
        [SerializeField] private GameObject Medieval_props_free_9Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_9.prefab".
        [SerializeField] private GameObject ef_23_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_normal.prefab".
        [SerializeField] private GameObject ef_23_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_blue.prefab".
        [SerializeField] private GameObject Ground_explosion_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_green.prefab".
        [SerializeField] private GameObject Lazer_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_purple.prefab".
        [SerializeField] private GameObject LevelTilemapPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Level\LevelTilemap.prefab".
        [SerializeField] private GameObject Medieval_props_free_6Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_6.prefab".
        [SerializeField] private GameObject Destruction_air_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_blue.prefab".
        [SerializeField] private GameObject ef_21_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_blue.prefab".
        [SerializeField] private GameObject FreezableWaterSourcePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\TemperatureManagable\FreezableWaterSource.prefab".
        [SerializeField] private GameObject Mine_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_purple.prefab".
        [SerializeField] private GameObject smoke_4Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_4.prefab".
        [SerializeField] private GameObject StartSpawnpointPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\SpawnPoint\StartSpawnpoint.prefab".
        [SerializeField] private GameObject DashDestroyableCrystalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\DashDestroyables\DashDestroyableCrystal.prefab".
        [SerializeField] private GameObject expl_02_03Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_03.prefab".
        [SerializeField] private GameObject ef_5_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_blue.prefab".
        [SerializeField] private GameObject ef_24_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_normal.prefab".
        [SerializeField] private GameObject ef_25_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_normal.prefab".
        [SerializeField] private GameObject Muzzle_dron_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_green.prefab".
        [SerializeField] private GameObject PressurePlatePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Senders\PressurePlate.prefab".
        [SerializeField] private GameObject Muzzle_tank_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_blue.prefab".
        [SerializeField] private GameObject ef_10_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_normal.prefab".
        [SerializeField] private GameObject TrapDoorPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Receivers\TrapDoor.prefab".
        [SerializeField] private GameObject EnemyPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Enemies\Enemy.prefab".
        [SerializeField] private GameObject ArrowPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Obstacle\Arrow.prefab".
        [SerializeField] private GameObject ef_25_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_purple.prefab".
        [SerializeField] private GameObject LevelControllerPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Level\LevelController.prefab".
        [SerializeField] private GameObject ef_16_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_green.prefab".
        [SerializeField] private GameObject ef_10_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_green.prefab".
        [SerializeField] private GameObject ef_8_2_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_purple.prefab".
        [SerializeField] private GameObject ef_19_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_purple.prefab".
        [SerializeField] private GameObject ef_7_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_purple.prefab".
        [SerializeField] private GameObject GeyserPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Geyser.prefab".
        [SerializeField] private GameObject Ricochet_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_blue.prefab".
        [SerializeField] private GameObject ef_9_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_purple.prefab".
        [SerializeField] private GameObject GrapplePointPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\GrapplePoint.prefab".
        [SerializeField] private GameObject Medieval_props_free_10Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_10.prefab".
        [SerializeField] private GameObject explosion_1Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_1.prefab".
        [SerializeField] private GameObject LoaderPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\LoadTrigger\Loader.prefab".
        [SerializeField] private GameObject ef_22_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_purple.prefab".
        [SerializeField] private GameObject FallingEnemyPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Enemies\FallingEnemy.prefab".
        [SerializeField] private GameObject Lazer_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_blue.prefab".
        [SerializeField] private GameObject ef_7_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_blue.prefab".
        [SerializeField] private GameObject Medieval_props_free_2Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_2.prefab".
        [SerializeField] private GameObject ef_14_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_blue.prefab".
        [SerializeField] private GameObject DoorPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Receivers\Door.prefab".
        [SerializeField] private GameObject expl_01_05Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_05.prefab".
        [SerializeField] private GameObject ef_22_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_blue.prefab".
        [SerializeField] private GameObject ef_18_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_purple.prefab".
        [SerializeField] private GameObject RTPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\RT.prefab".
        [SerializeField] private GameObject ef_21_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_normal.prefab".
        [SerializeField] private GameObject Muzzle_tank_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_purple.prefab".
        [SerializeField] private GameObject ef_6_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_green.prefab".
        [SerializeField] private GameObject firePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\fire.prefab".
        [SerializeField] private GameObject Muzzle_shotgun_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_normal.prefab".
        [SerializeField] private GameObject expl_01_02Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_02.prefab".
        [SerializeField] private GameObject ThinDestroyableVasePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\DashDestroyables\ThinDestroyableVase.prefab".
        [SerializeField] private GameObject ef_13_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_purple.prefab".
        [SerializeField] private GameObject ArtefactPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Senders\Artefact.prefab".
        [SerializeField] private GameObject expl_01_04Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_04.prefab".
        [SerializeField] private GameObject Flame_1Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_1.prefab".
        [SerializeField] private GameObject Trail_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_green.prefab".
        [SerializeField] private GameObject ef_24_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_green.prefab".
        [SerializeField] private GameObject JumpingEnemyPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Enemies\JumpingEnemy.prefab".
        [SerializeField] private GameObject ef_5_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_normal.prefab".
        [SerializeField] private GameObject ef_7_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_green.prefab".
        [SerializeField] private GameObject Rocket_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_green.prefab".
        [SerializeField] private GameObject fire_bigPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\fire_big.prefab".
        [SerializeField] private GameObject ef_10_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_blue.prefab".
        [SerializeField] private GameObject ef_15_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_green.prefab".
        [SerializeField] private GameObject ef_11_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_normal.prefab".
        [SerializeField] private GameObject Mine_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_normal.prefab".
        [SerializeField] private GameObject ef_1_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_1_normal.prefab".
        [SerializeField] private GameObject ef_12_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_blue.prefab".
        [SerializeField] private GameObject ef_7_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_normal.prefab".
        [SerializeField] private GameObject Medieval_props_free_5Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_5.prefab".
        [SerializeField] private GameObject ef_9_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_blue.prefab".
        // "Thumbstick 360rotation" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick 360rotation.prefab".
        // "Thumbstick Up-right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Up-right.prefab".
        [SerializeField] private GameObject ef_18_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_green.prefab".
        [SerializeField] private GameObject Medieval_props_free_11Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_11.prefab".
        [SerializeField] private GameObject explosion_2Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_2.prefab".
        [SerializeField] private GameObject plazmaPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\plazma.prefab".
        [SerializeField] private GameObject Ricochet_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_normal.prefab".
        [SerializeField] private GameObject SpawnHubPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\SpawnPoint\SpawnHub.prefab".
        [SerializeField] private GameObject ef_9_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_green.prefab".
        [SerializeField] private GameObject Trail_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_purple.prefab".
        [SerializeField] private GameObject ef_17_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_green.prefab".
        [SerializeField] private GameObject TemporaryPlatformPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Tiles\TilePalettes\TemporaryPlatform.prefab".
        [SerializeField] private GameObject Ricochet_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_green.prefab".
        // "D-pad Down" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\D-pad Down.prefab".
        // "D-pad Right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\D-pad Right.prefab".
        [SerializeField] private GameObject Muzzle_dron_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_normal.prefab".
        [SerializeField] private GameObject Medieval_props_free_1Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_1.prefab".
        [SerializeField] private GameObject ButtonPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\Button.prefab".
        [SerializeField] private GameObject smoke_1Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_1.prefab".
        [SerializeField] private GameObject StartPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Start.prefab".
        [SerializeField] private GameObject InformationSignPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\InformationSign.prefab".
        [SerializeField] private GameObject Flame_3Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_3.prefab".
        [SerializeField] private GameObject Ground_explosion_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_normal.prefab".
        // "X button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\X button.prefab".
        [SerializeField] private GameObject ef_22_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_normal.prefab".
        // "B button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\B button.prefab".
        [SerializeField] private GameObject Flame_4Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_4.prefab".
        [SerializeField] private GameObject ef_3_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_green.prefab".
        [SerializeField] private GameObject explosion_4Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_4.prefab".
        [SerializeField] private GameObject LevelCamerasPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Camera\LevelCameras.prefab".
        [SerializeField] private GameObject ef_8_1_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_normal.prefab".
        [SerializeField] private GameObject ef_6_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_blue.prefab".
        [SerializeField] private GameObject ef_5_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_green.prefab".
        [SerializeField] private GameObject ef_18_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_normal.prefab".
        // "Thumbstick Up-left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Up-left.prefab".
        [SerializeField] private GameObject ef_3_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_normal.prefab".
        [SerializeField] private GameObject ArrowSignPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\ArrowSign.prefab".
        [SerializeField] private GameObject smoke_5Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_5.prefab".
        [SerializeField] private GameObject ef_20_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_blue.prefab".
        [SerializeField] private GameObject LTPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\LT.prefab".
        // "Thumbstick Down-left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Down-left.prefab".
        [SerializeField] private GameObject ef_10_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_purple.prefab".
        [SerializeField] private GameObject Destruction_air_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_green.prefab".
        [SerializeField] private GameObject ef_24_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_blue.prefab".
        // "Thumbstick Up" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Up.prefab".
        [SerializeField] private GameObject ef_22_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_green.prefab".
        [SerializeField] private GameObject ef_24_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_purple.prefab".
        [SerializeField] private GameObject splashPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\splash.prefab".
        [SerializeField] private GameObject smoke_sidePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_side.prefab".
        [SerializeField] private GameObject TemperatureBarVisualPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\TemperatureBarVisual.prefab".
        [SerializeField] private GameObject LavaPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\TemperatureManagable\Lava.prefab".
        // "Y button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Y button.prefab".
        [SerializeField] private GameObject LevelTemplatePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Level\LevelTemplate.prefab".
        [SerializeField] private GameObject ground_explosionPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\ground_explosion.prefab".
        [SerializeField] private GameObject Medieval_props_free_8Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_8.prefab".
        [SerializeField] private GameObject ef_16_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_normal.prefab".
        [SerializeField] private GameObject expl_02_01Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_01.prefab".
        [SerializeField] private GameObject ef_5_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_purple.prefab".
        [SerializeField] private GameObject Muzzle_tank_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_green.prefab".
        [SerializeField] private GameObject ef_20_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_normal.prefab".
        [SerializeField] private GameObject ef_8_2_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_normal.prefab".
        [SerializeField] private GameObject Rocket_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_normal.prefab".
        [SerializeField] private GameObject Mine_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_blue.prefab".
        [SerializeField] private GameObject Flame_6Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_6.prefab".
        [SerializeField] private GameObject DeadSignPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\DeadSign.prefab".
        [SerializeField] private GameObject ef_16_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_blue.prefab".
        [SerializeField] private GameObject Trail_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_normal.prefab".
        [SerializeField] private GameObject ef_15_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_normal.prefab".
        [SerializeField] private GameObject ef_6_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_normal.prefab".
        [SerializeField] private GameObject Shotgun_hit_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_blue.prefab".
        [SerializeField] private GameObject NoSaveGamePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\NoSaveGame.prefab".
        // "A button" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\A button.prefab".
        [SerializeField] private GameObject Destruction_air_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_normal.prefab".
        [SerializeField] private GameObject Ground_explosion_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_blue.prefab".
        [SerializeField] private GameObject Lazer_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_normal.prefab".
        [SerializeField] private GameObject Muzzle_dron_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_blue.prefab".
        [SerializeField] private GameObject fire_smallPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\fire_small.prefab".
        [SerializeField] private GameObject smoke_3Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_3.prefab".
        [SerializeField] private GameObject ef_25_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_green.prefab".
        [SerializeField] private GameObject ef_3_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_purple.prefab".
        [SerializeField] private GameObject health_upPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\health_up.prefab".
        [SerializeField] private GameObject ef_14_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_normal.prefab".
        [SerializeField] private GameObject ef_8_1_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_green.prefab".
        [SerializeField] private GameObject ef_19_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_normal.prefab".
        [SerializeField] private GameObject BackPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Back.prefab".
        [SerializeField] private GameObject Shotgun_hit_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_purple.prefab".
        [SerializeField] private GameObject CrossSignPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\CrossSign.prefab".
        [SerializeField] private GameObject ef_12_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_normal.prefab".
        // "Thumbstick Left and Right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Left and Right.prefab".
        [SerializeField] private GameObject ef_19_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_blue.prefab".
        [SerializeField] private GameObject Lazer_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_green.prefab".
        [SerializeField] private GameObject expl_02_05Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_05.prefab".
        [SerializeField] private GameObject Muzzle_shotgun_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_green.prefab".
        [SerializeField] private GameObject ef_19_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_green.prefab".
        [SerializeField] private GameObject ef_21_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_purple.prefab".
        [SerializeField] private GameObject Shotgun_hit_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_normal.prefab".
        [SerializeField] private GameObject CratePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Crate.prefab".
        [SerializeField] private GameObject ef_15_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_blue.prefab".
        [SerializeField] private GameObject Flame_5Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_5.prefab".
        [SerializeField] private GameObject ef_8_2_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_green.prefab".
        [SerializeField] private GameObject ef_15_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_purple.prefab".
        [SerializeField] private GameObject ef_14_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_green.prefab".
        [SerializeField] private GameObject electricityPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\electricity.prefab".
        [SerializeField] private GameObject ef_4_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_blue.prefab".
        [SerializeField] private GameObject ef_9_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_normal.prefab".
        [SerializeField] private GameObject Muzzle_shotgun_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_purple.prefab".
        [SerializeField] private GameObject ef_17_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_normal.prefab".
        // "Main Camera" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\Main Camera.prefab".
        [SerializeField] private GameObject Medieval_props_free_12Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_12.prefab".
        [SerializeField] private GameObject explosion_3Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_3.prefab".
        [SerializeField] private GameObject flame_sidePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\flame_side.prefab".
        [SerializeField] private GameObject ready_attackPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\ready_attack.prefab".
        [SerializeField] private GameObject ef_13_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_blue.prefab".
        [SerializeField] private GameObject expl_01_03Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_03.prefab".
        [SerializeField] private GameObject ef_16_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_purple.prefab".
        [SerializeField] private GameObject EndLevelPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\LoadTrigger\EndLevel.prefab".
        [SerializeField] private GameObject ef_17_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_purple.prefab".
        [SerializeField] private GameObject AchievementPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\Achievement.prefab".
        [SerializeField] private GameObject ef_20_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_purple.prefab".
        [SerializeField] private GameObject ef_3_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_blue.prefab".
        [SerializeField] private GameObject FatDestroyableVasePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\DashDestroyables\FatDestroyableVase.prefab".
        [SerializeField] private GameObject ef_4_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_purple.prefab".
        [SerializeField] private GameObject ef_8_2_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_blue.prefab".
        [SerializeField] private GameObject ef_23_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_purple.prefab".
        [SerializeField] private GameObject doorEndPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\LoadTrigger\doorEnd.prefab".
        [SerializeField] private GameObject ef_12_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_purple.prefab".
        [SerializeField] private GameObject launcherPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\launcher.prefab".
        [SerializeField] private GameObject ef_11_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_purple.prefab".
        [SerializeField] private GameObject expl_02_04Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_04.prefab".
        [SerializeField] private GameObject TorchPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\Torch.prefab".
        // "Thumbstick Up 1" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Up 1.prefab".
        [SerializeField] private GameObject Muzzle_shotgun_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_blue.prefab".
        [SerializeField] private GameObject granadePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\granade.prefab".
        // "Thumbstick Right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Right.prefab".
        [SerializeField] private GameObject Destruction_air_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_purple.prefab".
        [SerializeField] private GameObject Rocket_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_blue.prefab".
        [SerializeField] private GameObject ArrowTrapPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Obstacle\ArrowTrap.prefab".
        [SerializeField] private GameObject ef_12_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_green.prefab".
        [SerializeField] private GameObject ef_6_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_purple.prefab".
        [SerializeField] private GameObject Muzzle_dron_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_purple.prefab".
        [SerializeField] private GameObject Shotgun_hit_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_green.prefab".
        [SerializeField] private GameObject Rocket_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_purple.prefab".
        // "D-pad Up" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\D-pad Up.prefab".
        [SerializeField] private GameObject MovingPlatformGroupPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Receivers\MovingPlatformGroup.prefab".
        // "Thumbstick Down" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Down.prefab".
        [SerializeField] private GameObject Medieval_props_free_4Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_4.prefab".
        [SerializeField] private GameObject Trail_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_blue.prefab".
        [SerializeField] private GameObject TutorialPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Tiles\TilePalettes\Tutorial.prefab".
        [SerializeField] private GameObject ef_2_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_2_normal.prefab".
        [SerializeField] private GameObject ef_4_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_normal.prefab".
        [SerializeField] private GameObject Ricochet_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_purple.prefab".
        [SerializeField] private GameObject ef_11_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_green.prefab".
        [SerializeField] private GameObject ef_13_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_normal.prefab".
        // "Thumbstick Left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Left.prefab".
        [SerializeField] private GameObject AbilityCooldownPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\AbilityCooldown.prefab".
        [SerializeField] private GameObject ef_8_1_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_purple.prefab".
        [SerializeField] private GameObject ef_21_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_green.prefab".
        [SerializeField] private GameObject Mine_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_green.prefab".
        [SerializeField] private GameObject ef_25_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_blue.prefab".
        [SerializeField] private GameObject smoke_2Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_2.prefab".
        [SerializeField] private GameObject Ground_explosion_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_purple.prefab".
        [SerializeField] private GameObject ef_8_1_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_blue.prefab".
        // "Thumbstick Down-right" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Thumbstick Down-right.prefab".
        [SerializeField] private GameObject SaveGamePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\SaveGame.prefab".
        [SerializeField] private GameObject expl_02_02Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_02.prefab".
        [SerializeField] private GameObject Muzzle_tank_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_normal.prefab".
        [SerializeField] private GameObject explosion_5Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_5.prefab".
        [SerializeField] private GameObject MeltableIceBlockPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\TemperatureManagable\MeltableIceBlock.prefab".
        [SerializeField] private GameObject CharacterPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Character.prefab".
        [SerializeField] private GameObject ef_14_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_purple.prefab".
        // "D-pad Left" is invalid. See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\D-pad Left.prefab".
        [SerializeField] private GameObject SaveSpaceChoicePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\SaveSpaceChoice.prefab".
        [SerializeField] private GameObject ef_17_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_blue.prefab".
        [SerializeField] private GameObject ef_4_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_green.prefab".
        [SerializeField] private GameObject Medieval_props_free_3Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_3.prefab".
    
        public static GameObject ef_20_green => instance.ef_20_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_green.prefab".
        public static GameObject ef_13_green => instance.ef_13_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_green.prefab".
        public static GameObject WanderingEnemy => instance.WanderingEnemyPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Enemies\WanderingEnemy.prefab".
        public static GameObject Lever => instance.LeverPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Senders\Lever.prefab".
        public static GameObject ef_11_blue => instance.ef_11_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_blue.prefab".
        public static GameObject Player => instance.PlayerPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Player\Player.prefab".
        public static GameObject RB => instance.RBPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\RB.prefab".
        public static GameObject Medieval_props_free_21 => instance.Medieval_props_free_21Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_21.prefab".
        public static GameObject ef_23_green => instance.ef_23_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_green.prefab".
        public static GameObject Flame_2 => instance.Flame_2Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_2.prefab".
        public static GameObject expl_01_01 => instance.expl_01_01Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_01.prefab".
        public static GameObject burn_effect => instance.burn_effectPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\burn_effect.prefab".
        public static GameObject ef_18_blue => instance.ef_18_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_blue.prefab".
        public static GameObject LB => instance.LBPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\LB.prefab".
        public static GameObject ricochet => instance.ricochetPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\ricochet.prefab".
        public static GameObject Medieval_props_free_9 => instance.Medieval_props_free_9Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_9.prefab".
        public static GameObject ef_23_normal => instance.ef_23_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_normal.prefab".
        public static GameObject ef_23_blue => instance.ef_23_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_blue.prefab".
        public static GameObject Ground_explosion_green => instance.Ground_explosion_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_green.prefab".
        public static GameObject Lazer_purple => instance.Lazer_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_purple.prefab".
        public static GameObject LevelTilemap => instance.LevelTilemapPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Level\LevelTilemap.prefab".
        public static GameObject Medieval_props_free_6 => instance.Medieval_props_free_6Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_6.prefab".
        public static GameObject Destruction_air_blue => instance.Destruction_air_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_blue.prefab".
        public static GameObject ef_21_blue => instance.ef_21_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_blue.prefab".
        public static GameObject FreezableWaterSource => instance.FreezableWaterSourcePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\TemperatureManagable\FreezableWaterSource.prefab".
        public static GameObject Mine_purple => instance.Mine_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_purple.prefab".
        public static GameObject smoke_4 => instance.smoke_4Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_4.prefab".
        public static GameObject StartSpawnpoint => instance.StartSpawnpointPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\SpawnPoint\StartSpawnpoint.prefab".
        public static GameObject DashDestroyableCrystal => instance.DashDestroyableCrystalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\DashDestroyables\DashDestroyableCrystal.prefab".
        public static GameObject expl_02_03 => instance.expl_02_03Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_03.prefab".
        public static GameObject ef_5_blue => instance.ef_5_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_blue.prefab".
        public static GameObject ef_24_normal => instance.ef_24_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_normal.prefab".
        public static GameObject ef_25_normal => instance.ef_25_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_normal.prefab".
        public static GameObject Muzzle_dron_green => instance.Muzzle_dron_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_green.prefab".
        public static GameObject PressurePlate => instance.PressurePlatePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Senders\PressurePlate.prefab".
        public static GameObject Muzzle_tank_blue => instance.Muzzle_tank_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_blue.prefab".
        public static GameObject ef_10_normal => instance.ef_10_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_normal.prefab".
        public static GameObject TrapDoor => instance.TrapDoorPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Receivers\TrapDoor.prefab".
        public static GameObject Enemy => instance.EnemyPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Enemies\Enemy.prefab".
        public static GameObject Arrow => instance.ArrowPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Obstacle\Arrow.prefab".
        public static GameObject ef_25_purple => instance.ef_25_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_purple.prefab".
        public static GameObject LevelController => instance.LevelControllerPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Level\LevelController.prefab".
        public static GameObject ef_16_green => instance.ef_16_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_green.prefab".
        public static GameObject ef_10_green => instance.ef_10_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_green.prefab".
        public static GameObject ef_8_2_purple => instance.ef_8_2_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_purple.prefab".
        public static GameObject ef_19_purple => instance.ef_19_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_purple.prefab".
        public static GameObject ef_7_purple => instance.ef_7_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_purple.prefab".
        public static GameObject Geyser => instance.GeyserPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Geyser.prefab".
        public static GameObject Ricochet_blue => instance.Ricochet_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_blue.prefab".
        public static GameObject ef_9_purple => instance.ef_9_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_purple.prefab".
        public static GameObject GrapplePoint => instance.GrapplePointPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\GrapplePoint.prefab".
        public static GameObject Medieval_props_free_10 => instance.Medieval_props_free_10Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_10.prefab".
        public static GameObject explosion_1 => instance.explosion_1Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_1.prefab".
        public static GameObject Loader => instance.LoaderPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\LoadTrigger\Loader.prefab".
        public static GameObject ef_22_purple => instance.ef_22_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_purple.prefab".
        public static GameObject FallingEnemy => instance.FallingEnemyPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Enemies\FallingEnemy.prefab".
        public static GameObject Lazer_blue => instance.Lazer_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_blue.prefab".
        public static GameObject ef_7_blue => instance.ef_7_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_blue.prefab".
        public static GameObject Medieval_props_free_2 => instance.Medieval_props_free_2Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_2.prefab".
        public static GameObject ef_14_blue => instance.ef_14_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_blue.prefab".
        public static GameObject Door => instance.DoorPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Receivers\Door.prefab".
        public static GameObject expl_01_05 => instance.expl_01_05Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_05.prefab".
        public static GameObject ef_22_blue => instance.ef_22_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_blue.prefab".
        public static GameObject ef_18_purple => instance.ef_18_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_purple.prefab".
        public static GameObject RT => instance.RTPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\RT.prefab".
        public static GameObject ef_21_normal => instance.ef_21_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_normal.prefab".
        public static GameObject Muzzle_tank_purple => instance.Muzzle_tank_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_purple.prefab".
        public static GameObject ef_6_green => instance.ef_6_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_green.prefab".
        public static GameObject fire => instance.firePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\fire.prefab".
        public static GameObject Muzzle_shotgun_normal => instance.Muzzle_shotgun_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_normal.prefab".
        public static GameObject expl_01_02 => instance.expl_01_02Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_02.prefab".
        public static GameObject ThinDestroyableVase => instance.ThinDestroyableVasePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\DashDestroyables\ThinDestroyableVase.prefab".
        public static GameObject ef_13_purple => instance.ef_13_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_purple.prefab".
        public static GameObject Artefact => instance.ArtefactPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Senders\Artefact.prefab".
        public static GameObject expl_01_04 => instance.expl_01_04Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_04.prefab".
        public static GameObject Flame_1 => instance.Flame_1Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_1.prefab".
        public static GameObject Trail_green => instance.Trail_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_green.prefab".
        public static GameObject ef_24_green => instance.ef_24_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_green.prefab".
        public static GameObject JumpingEnemy => instance.JumpingEnemyPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Enemies\JumpingEnemy.prefab".
        public static GameObject ef_5_normal => instance.ef_5_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_normal.prefab".
        public static GameObject ef_7_green => instance.ef_7_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_green.prefab".
        public static GameObject Rocket_green => instance.Rocket_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_green.prefab".
        public static GameObject fire_big => instance.fire_bigPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\fire_big.prefab".
        public static GameObject ef_10_blue => instance.ef_10_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_blue.prefab".
        public static GameObject ef_15_green => instance.ef_15_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_green.prefab".
        public static GameObject ef_11_normal => instance.ef_11_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_normal.prefab".
        public static GameObject Mine_normal => instance.Mine_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_normal.prefab".
        public static GameObject ef_1_normal => instance.ef_1_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_1_normal.prefab".
        public static GameObject ef_12_blue => instance.ef_12_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_blue.prefab".
        public static GameObject ef_7_normal => instance.ef_7_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_7_normal.prefab".
        public static GameObject Medieval_props_free_5 => instance.Medieval_props_free_5Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_5.prefab".
        public static GameObject ef_9_blue => instance.ef_9_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_blue.prefab".
        public static GameObject ef_18_green => instance.ef_18_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_green.prefab".
        public static GameObject Medieval_props_free_11 => instance.Medieval_props_free_11Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_11.prefab".
        public static GameObject explosion_2 => instance.explosion_2Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_2.prefab".
        public static GameObject plazma => instance.plazmaPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\plazma.prefab".
        public static GameObject Ricochet_normal => instance.Ricochet_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_normal.prefab".
        public static GameObject SpawnHub => instance.SpawnHubPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\SpawnPoint\SpawnHub.prefab".
        public static GameObject ef_9_green => instance.ef_9_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_green.prefab".
        public static GameObject Trail_purple => instance.Trail_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_purple.prefab".
        public static GameObject ef_17_green => instance.ef_17_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_green.prefab".
        public static GameObject TemporaryPlatform => instance.TemporaryPlatformPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Tiles\TilePalettes\TemporaryPlatform.prefab".
        public static GameObject Ricochet_green => instance.Ricochet_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_green.prefab".
        public static GameObject Muzzle_dron_normal => instance.Muzzle_dron_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_normal.prefab".
        public static GameObject Medieval_props_free_1 => instance.Medieval_props_free_1Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_1.prefab".
        public static GameObject Button => instance.ButtonPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\Button.prefab".
        public static GameObject smoke_1 => instance.smoke_1Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_1.prefab".
        public static GameObject Start => instance.StartPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Start.prefab".
        public static GameObject InformationSign => instance.InformationSignPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\InformationSign.prefab".
        public static GameObject Flame_3 => instance.Flame_3Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_3.prefab".
        public static GameObject Ground_explosion_normal => instance.Ground_explosion_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_normal.prefab".
        public static GameObject ef_22_normal => instance.ef_22_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_normal.prefab".
        public static GameObject Flame_4 => instance.Flame_4Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_4.prefab".
        public static GameObject ef_3_green => instance.ef_3_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_green.prefab".
        public static GameObject explosion_4 => instance.explosion_4Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_4.prefab".
        public static GameObject LevelCameras => instance.LevelCamerasPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Camera\LevelCameras.prefab".
        public static GameObject ef_8_1_normal => instance.ef_8_1_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_normal.prefab".
        public static GameObject ef_6_blue => instance.ef_6_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_blue.prefab".
        public static GameObject ef_5_green => instance.ef_5_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_green.prefab".
        public static GameObject ef_18_normal => instance.ef_18_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_18_normal.prefab".
        public static GameObject ef_3_normal => instance.ef_3_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_normal.prefab".
        public static GameObject ArrowSign => instance.ArrowSignPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\ArrowSign.prefab".
        public static GameObject smoke_5 => instance.smoke_5Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_5.prefab".
        public static GameObject ef_20_blue => instance.ef_20_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_blue.prefab".
        public static GameObject LT => instance.LTPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\LT.prefab".
        public static GameObject ef_10_purple => instance.ef_10_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_10_purple.prefab".
        public static GameObject Destruction_air_green => instance.Destruction_air_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_green.prefab".
        public static GameObject ef_24_blue => instance.ef_24_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_blue.prefab".
        public static GameObject ef_22_green => instance.ef_22_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_22_green.prefab".
        public static GameObject ef_24_purple => instance.ef_24_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_24_purple.prefab".
        public static GameObject splash => instance.splashPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\splash.prefab".
        public static GameObject smoke_side => instance.smoke_sidePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_side.prefab".
        public static GameObject TemperatureBarVisual => instance.TemperatureBarVisualPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\TemperatureBarVisual.prefab".
        public static GameObject Lava => instance.LavaPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\TemperatureManagable\Lava.prefab".
        public static GameObject LevelTemplate => instance.LevelTemplatePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Level\LevelTemplate.prefab".
        public static GameObject ground_explosion => instance.ground_explosionPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\ground_explosion.prefab".
        public static GameObject Medieval_props_free_8 => instance.Medieval_props_free_8Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_8.prefab".
        public static GameObject ef_16_normal => instance.ef_16_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_normal.prefab".
        public static GameObject expl_02_01 => instance.expl_02_01Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_01.prefab".
        public static GameObject ef_5_purple => instance.ef_5_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_5_purple.prefab".
        public static GameObject Muzzle_tank_green => instance.Muzzle_tank_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_green.prefab".
        public static GameObject ef_20_normal => instance.ef_20_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_normal.prefab".
        public static GameObject ef_8_2_normal => instance.ef_8_2_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_normal.prefab".
        public static GameObject Rocket_normal => instance.Rocket_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_normal.prefab".
        public static GameObject Mine_blue => instance.Mine_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_blue.prefab".
        public static GameObject Flame_6 => instance.Flame_6Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_6.prefab".
        public static GameObject DeadSign => instance.DeadSignPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\DeadSign.prefab".
        public static GameObject ef_16_blue => instance.ef_16_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_blue.prefab".
        public static GameObject Trail_normal => instance.Trail_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_normal.prefab".
        public static GameObject ef_15_normal => instance.ef_15_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_normal.prefab".
        public static GameObject ef_6_normal => instance.ef_6_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_normal.prefab".
        public static GameObject Shotgun_hit_blue => instance.Shotgun_hit_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_blue.prefab".
        public static GameObject NoSaveGame => instance.NoSaveGamePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\NoSaveGame.prefab".
        public static GameObject Destruction_air_normal => instance.Destruction_air_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_normal.prefab".
        public static GameObject Ground_explosion_blue => instance.Ground_explosion_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_blue.prefab".
        public static GameObject Lazer_normal => instance.Lazer_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_normal.prefab".
        public static GameObject Muzzle_dron_blue => instance.Muzzle_dron_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_blue.prefab".
        public static GameObject fire_small => instance.fire_smallPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\fire_small.prefab".
        public static GameObject smoke_3 => instance.smoke_3Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_3.prefab".
        public static GameObject ef_25_green => instance.ef_25_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_green.prefab".
        public static GameObject ef_3_purple => instance.ef_3_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_purple.prefab".
        public static GameObject health_up => instance.health_upPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\health_up.prefab".
        public static GameObject ef_14_normal => instance.ef_14_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_normal.prefab".
        public static GameObject ef_8_1_green => instance.ef_8_1_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_green.prefab".
        public static GameObject ef_19_normal => instance.ef_19_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_normal.prefab".
        public static GameObject Back => instance.BackPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Xbox Controller\Prefab\Back.prefab".
        public static GameObject Shotgun_hit_purple => instance.Shotgun_hit_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_purple.prefab".
        public static GameObject CrossSign => instance.CrossSignPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\CrossSign.prefab".
        public static GameObject ef_12_normal => instance.ef_12_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_normal.prefab".
        public static GameObject ef_19_blue => instance.ef_19_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_blue.prefab".
        public static GameObject Lazer_green => instance.Lazer_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Lazer_green.prefab".
        public static GameObject expl_02_05 => instance.expl_02_05Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_05.prefab".
        public static GameObject Muzzle_shotgun_green => instance.Muzzle_shotgun_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_green.prefab".
        public static GameObject ef_19_green => instance.ef_19_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_19_green.prefab".
        public static GameObject ef_21_purple => instance.ef_21_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_purple.prefab".
        public static GameObject Shotgun_hit_normal => instance.Shotgun_hit_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_normal.prefab".
        public static GameObject Crate => instance.CratePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Crate.prefab".
        public static GameObject ef_15_blue => instance.ef_15_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_blue.prefab".
        public static GameObject Flame_5 => instance.Flame_5Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Flames\Flame_5.prefab".
        public static GameObject ef_8_2_green => instance.ef_8_2_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_green.prefab".
        public static GameObject ef_15_purple => instance.ef_15_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_15_purple.prefab".
        public static GameObject ef_14_green => instance.ef_14_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_green.prefab".
        public static GameObject electricity => instance.electricityPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\electricity.prefab".
        public static GameObject ef_4_blue => instance.ef_4_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_blue.prefab".
        public static GameObject ef_9_normal => instance.ef_9_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_9_normal.prefab".
        public static GameObject Muzzle_shotgun_purple => instance.Muzzle_shotgun_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_purple.prefab".
        public static GameObject ef_17_normal => instance.ef_17_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_normal.prefab".
        public static GameObject Medieval_props_free_12 => instance.Medieval_props_free_12Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_12.prefab".
        public static GameObject explosion_3 => instance.explosion_3Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_3.prefab".
        public static GameObject flame_side => instance.flame_sidePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\flame_side.prefab".
        public static GameObject ready_attack => instance.ready_attackPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\ready_attack.prefab".
        public static GameObject ef_13_blue => instance.ef_13_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_blue.prefab".
        public static GameObject expl_01_03 => instance.expl_01_03Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_01_03.prefab".
        public static GameObject ef_16_purple => instance.ef_16_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_16_purple.prefab".
        public static GameObject EndLevel => instance.EndLevelPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\LoadTrigger\EndLevel.prefab".
        public static GameObject ef_17_purple => instance.ef_17_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_purple.prefab".
        public static GameObject Achievement => instance.AchievementPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\Achievement.prefab".
        public static GameObject ef_20_purple => instance.ef_20_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_20_purple.prefab".
        public static GameObject ef_3_blue => instance.ef_3_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_3_blue.prefab".
        public static GameObject FatDestroyableVase => instance.FatDestroyableVasePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\DashDestroyables\FatDestroyableVase.prefab".
        public static GameObject ef_4_purple => instance.ef_4_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_purple.prefab".
        public static GameObject ef_8_2_blue => instance.ef_8_2_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_2_blue.prefab".
        public static GameObject ef_23_purple => instance.ef_23_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_23_purple.prefab".
        public static GameObject doorEnd => instance.doorEndPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\LoadTrigger\doorEnd.prefab".
        public static GameObject ef_12_purple => instance.ef_12_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_purple.prefab".
        public static GameObject launcher => instance.launcherPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\launcher.prefab".
        public static GameObject ef_11_purple => instance.ef_11_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_purple.prefab".
        public static GameObject expl_02_04 => instance.expl_02_04Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_04.prefab".
        public static GameObject Torch => instance.TorchPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Visual\Torch.prefab".
        public static GameObject Muzzle_shotgun_blue => instance.Muzzle_shotgun_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_shotgun_blue.prefab".
        public static GameObject granade => instance.granadePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\18 effects\granade.prefab".
        public static GameObject Destruction_air_purple => instance.Destruction_air_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Destruction_air_purple.prefab".
        public static GameObject Rocket_blue => instance.Rocket_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_blue.prefab".
        public static GameObject ArrowTrap => instance.ArrowTrapPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Obstacle\ArrowTrap.prefab".
        public static GameObject ef_12_green => instance.ef_12_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_12_green.prefab".
        public static GameObject ef_6_purple => instance.ef_6_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_6_purple.prefab".
        public static GameObject Muzzle_dron_purple => instance.Muzzle_dron_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_dron_purple.prefab".
        public static GameObject Shotgun_hit_green => instance.Shotgun_hit_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Shotgun_hit_green.prefab".
        public static GameObject Rocket_purple => instance.Rocket_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Rocket_purple.prefab".
        public static GameObject MovingPlatformGroup => instance.MovingPlatformGroupPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Interactables\Receivers\MovingPlatformGroup.prefab".
        public static GameObject Medieval_props_free_4 => instance.Medieval_props_free_4Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_4.prefab".
        public static GameObject Trail_blue => instance.Trail_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Trail_blue.prefab".
        public static GameObject Tutorial => instance.TutorialPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Tiles\TilePalettes\Tutorial.prefab".
        public static GameObject ef_2_normal => instance.ef_2_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_2_normal.prefab".
        public static GameObject ef_4_normal => instance.ef_4_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_normal.prefab".
        public static GameObject Ricochet_purple => instance.Ricochet_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ricochet_purple.prefab".
        public static GameObject ef_11_green => instance.ef_11_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_11_green.prefab".
        public static GameObject ef_13_normal => instance.ef_13_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_13_normal.prefab".
        public static GameObject AbilityCooldown => instance.AbilityCooldownPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\AbilityCooldown.prefab".
        public static GameObject ef_8_1_purple => instance.ef_8_1_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_purple.prefab".
        public static GameObject ef_21_green => instance.ef_21_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_21_green.prefab".
        public static GameObject Mine_green => instance.Mine_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Mine_green.prefab".
        public static GameObject ef_25_blue => instance.ef_25_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_25_blue.prefab".
        public static GameObject smoke_2 => instance.smoke_2Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\smoke_2.prefab".
        public static GameObject Ground_explosion_purple => instance.Ground_explosion_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Ground_explosion_purple.prefab".
        public static GameObject ef_8_1_blue => instance.ef_8_1_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_8_1_blue.prefab".
        public static GameObject SaveGame => instance.SaveGamePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\LoadSave\SaveGame.prefab".
        public static GameObject expl_02_02 => instance.expl_02_02Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\Explosions\expl_02_02.prefab".
        public static GameObject Muzzle_tank_normal => instance.Muzzle_tank_normalPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\15 effects\Muzzle_tank_normal.prefab".
        public static GameObject explosion_5 => instance.explosion_5Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\19 effects\explosion_5.prefab".
        public static GameObject MeltableIceBlock => instance.MeltableIceBlockPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\TemperatureManagable\MeltableIceBlock.prefab".
        public static GameObject Character => instance.CharacterPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\Actors\Character\Character.prefab".
        public static GameObject ef_14_purple => instance.ef_14_purplePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_14_purple.prefab".
        public static GameObject SaveSpaceChoice => instance.SaveSpaceChoicePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Prefabs\UI\SaveSpaceChoice.prefab".
        public static GameObject ef_17_blue => instance.ef_17_bluePrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_17_blue.prefab".
        public static GameObject ef_4_green => instance.ef_4_greenPrefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\118 sprite effects bundle\25 sprite effects\ef_4_green.prefab".
        public static GameObject Medieval_props_free_3 => instance.Medieval_props_free_3Prefab; // See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Visuals\Medieval_pixel_art_asset_FREE\Prefabs\Props_free\Medieval_props_free_3.prefab".

        public Prefabs()
        {
            instance = this;
        }
    }
}