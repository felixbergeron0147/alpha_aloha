// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-11-27 16:19:25

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using System;
using UnityEngine;

namespace Harmony
{
    public static partial class Finder
    {
        private static Game.DeadMenu findableDeadMenu = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\UI\DeadMenu.cs".
        public static Game.DeadMenu DeadMenu
        {
            get
            {
                if (!findableDeadMenu)
                {
                    findableDeadMenu = FindWithTag<Game.DeadMenu>(Tags.DeadMenu);
                }
                return findableDeadMenu;
            }
        }
    
        private static Game.UserInterface findableUserInterface = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\UI\UserInterface.cs".
        public static Game.UserInterface UserInterface
        {
            get
            {
                if (!findableUserInterface)
                {
                    findableUserInterface = FindWithTag<Game.UserInterface>(Tags.UICanvas);
                }
                return findableUserInterface;
            }
        }
    
        private static Game.GameMemory findableGameMemory = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Mode\Main\GameMemory.cs".
        public static Game.GameMemory GameMemory
        {
            get
            {
                if (!findableGameMemory)
                {
                    findableGameMemory = FindWithTag<Game.GameMemory>(Tags.MainController);
                }
                return findableGameMemory;
            }
        }
    
        private static Game.Main findableMain = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Mode\Main\Main.cs".
        public static Game.Main Main
        {
            get
            {
                if (!findableMain)
                {
                    findableMain = FindWithTag<Game.Main>(Tags.MainController);
                }
                return findableMain;
            }
        }
    
        private static Game.LevelController findableLevelController = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Mode\Level\LevelController.cs".
        public static Game.LevelController LevelController
        {
            get
            {
                if (!findableLevelController)
                {
                    findableLevelController = FindWithTag<Game.LevelController>(Tags.LevelController);
                }
                return findableLevelController;
            }
        }
    
        private static Game.HomeController findableHomeController = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Mode\Home\HomeController.cs".
        public static Game.HomeController HomeController
        {
            get
            {
                if (!findableHomeController)
                {
                    findableHomeController = FindWithTag<Game.HomeController>(Tags.HomeController);
                }
                return findableHomeController;
            }
        }
    
        private static Game.AchievementTitle findableAchievementTitle = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Mode\Home\Achievement\AchievementTitle.cs".
        public static Game.AchievementTitle AchievementTitle
        {
            get
            {
                if (!findableAchievementTitle)
                {
                    findableAchievementTitle = FindWithTag<Game.AchievementTitle>(Tags.GameController);
                }
                return findableAchievementTitle;
            }
        }
    
        private static Game.GameController findableGameController = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Mode\Game\GameController.cs".
        public static Game.GameController GameController
        {
            get
            {
                if (!findableGameController)
                {
                    findableGameController = FindWithTag<Game.GameController>(Tags.GameController);
                }
                return findableGameController;
            }
        }
    
        private static Game.Inputs findableInputs = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Input\Inputs.cs".
        public static Game.Inputs Inputs
        {
            get
            {
                if (!findableInputs)
                {
                    findableInputs = FindWithTag<Game.Inputs>(Tags.MainController);
                }
                return findableInputs;
            }
        }
    
        private static Game.OnAchievementUnlockedEventChannel findableOnAchievementUnlockedEventChannel = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Events\OnAchievementUnlockedEventChannel.cs".
        public static Game.OnAchievementUnlockedEventChannel OnAchievementUnlockedEventChannel
        {
            get
            {
                if (!findableOnAchievementUnlockedEventChannel)
                {
                    findableOnAchievementUnlockedEventChannel = FindWithTag<Game.OnAchievementUnlockedEventChannel>(Tags.GameController);
                }
                return findableOnAchievementUnlockedEventChannel;
            }
        }
    
        private static Game.OnActionActivatedChannel findableOnActionActivatedChannel = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Events\OnActionActivatedChannel.cs".
        public static Game.OnActionActivatedChannel OnActionActivatedChannel
        {
            get
            {
                if (!findableOnActionActivatedChannel)
                {
                    findableOnActionActivatedChannel = FindWithTag<Game.OnActionActivatedChannel>(Tags.GameController);
                }
                return findableOnActionActivatedChannel;
            }
        }
    
        private static Game.OnArtefactCollectEventChannel findableOnArtefactCollectEventChannel = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Events\OnArtefactCollectEventChannel.cs".
        public static Game.OnArtefactCollectEventChannel OnArtefactCollectEventChannel
        {
            get
            {
                if (!findableOnArtefactCollectEventChannel)
                {
                    findableOnArtefactCollectEventChannel = FindWithTag<Game.OnArtefactCollectEventChannel>(Tags.GameController);
                }
                return findableOnArtefactCollectEventChannel;
            }
        }
    
        private static Game.OnLevelEndEventChannel findableOnLevelEndEventChannel = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Events\OnLevelEndEventChannel.cs".
        public static Game.OnLevelEndEventChannel OnLevelEndEventChannel
        {
            get
            {
                if (!findableOnLevelEndEventChannel)
                {
                    findableOnLevelEndEventChannel = FindWithTag<Game.OnLevelEndEventChannel>(Tags.GameController);
                }
                return findableOnLevelEndEventChannel;
            }
        }
    
        private static Game.OnPlayerDeathEventChannel findableOnPlayerDeathEventChannel = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Events\OnPlayerDeathEventChannel.cs".
        public static Game.OnPlayerDeathEventChannel OnPlayerDeathEventChannel
        {
            get
            {
                if (!findableOnPlayerDeathEventChannel)
                {
                    findableOnPlayerDeathEventChannel = FindWithTag<Game.OnPlayerDeathEventChannel>(Tags.GameController);
                }
                return findableOnPlayerDeathEventChannel;
            }
        }
    
        private static Game.OnPlayerLifeModifiedEventChannel findableOnPlayerLifeModifiedEventChannel = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Events\OnPlayerLifeModifiedEventChannel.cs".
        public static Game.OnPlayerLifeModifiedEventChannel OnPlayerLifeModifiedEventChannel
        {
            get
            {
                if (!findableOnPlayerLifeModifiedEventChannel)
                {
                    findableOnPlayerLifeModifiedEventChannel = FindWithTag<Game.OnPlayerLifeModifiedEventChannel>(Tags.GameController);
                }
                return findableOnPlayerLifeModifiedEventChannel;
            }
        }
    
        private static Game.OnPlayerTempStateChangedEventChannel findableOnPlayerTempStateChangedEventChannel = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Events\OnPlayerTempStateChangedEventChannel.cs".
        public static Game.OnPlayerTempStateChangedEventChannel OnPlayerTempStateChangedEventChannel
        {
            get
            {
                if (!findableOnPlayerTempStateChangedEventChannel)
                {
                    findableOnPlayerTempStateChangedEventChannel = FindWithTag<Game.OnPlayerTempStateChangedEventChannel>(Tags.GameController);
                }
                return findableOnPlayerTempStateChangedEventChannel;
            }
        }
    
        private static Game.OnTemperatureModifiedEventChannel findableOnTemperatureModifiedEventChannel = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Events\OnTemperatureModifiedEventChannel.cs".
        public static Game.OnTemperatureModifiedEventChannel OnTemperatureModifiedEventChannel
        {
            get
            {
                if (!findableOnTemperatureModifiedEventChannel)
                {
                    findableOnTemperatureModifiedEventChannel = FindWithTag<Game.OnTemperatureModifiedEventChannel>(Tags.GameController);
                }
                return findableOnTemperatureModifiedEventChannel;
            }
        }
    
        private static Game.SceneLoadSetting findableSceneLoadSetting = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Common\SceneManager\SceneLoadSetting.cs".
        public static Game.SceneLoadSetting SceneLoadSetting
        {
            get
            {
                if (!findableSceneLoadSetting)
                {
                    findableSceneLoadSetting = FindWithTag<Game.SceneLoadSetting>(Tags.MainController);
                }
                return findableSceneLoadSetting;
            }
        }
    
        private static Game.SaveSystem findableSaveSystem = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Common\SaveSystem\SaveSystem.cs".
        public static Game.SaveSystem SaveSystem
        {
            get
            {
                if (!findableSaveSystem)
                {
                    findableSaveSystem = FindWithTag<Game.SaveSystem>(Tags.MainController);
                }
                return findableSaveSystem;
            }
        }
    
        private static Game.ArrowFactory findableArrowFactory = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Common\ObjectPools\ArrowFactory.cs".
        public static Game.ArrowFactory ArrowFactory
        {
            get
            {
                if (!findableArrowFactory)
                {
                    findableArrowFactory = FindWithTag<Game.ArrowFactory>(Tags.ObjectPool);
                }
                return findableArrowFactory;
            }
        }
    
        private static Game.Player findablePlayer = null; //See "D:\félix\Desktop\Cégep\ProSynRelate\projet_synthese_2020\Assets\Scripts\Play\Actor\Character\Player\Player.cs".
        public static Game.Player Player
        {
            get
            {
                if (!findablePlayer)
                {
                    findablePlayer = FindWithTag<Game.Player>(Tags.Player);
                }
                return findablePlayer;
            }
        }
    
        public static T FindWithTag<T>(string tag) where T : class
        {
            return GameObject.FindWithTag(tag)?.GetComponent<T>();
        }
    }
}