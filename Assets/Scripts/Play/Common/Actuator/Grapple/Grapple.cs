﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Félix GC
    [RequireComponent(typeof(TriggerSensor2D))]
    public class Grapple : MonoBehaviour
    {
        [SerializeField] private float grappleSpeed = 40.0f;
        [SerializeField][Min(0.01f)] private float grappleTolerance = 0.5f;

        private ISensor<GrapplePoint> grappleSensor;
        private GrapplePoint nearestGrapplePoint;
        private List<RaycastHit2D> groundHits;
        private ContactFilter2D groundTerrainFilter;

        private Transform ParentTransform => transform.parent.transform;
        
        private void Awake()
        {
            grappleSensor = GetComponent<TriggerSensor2D>().For<GrapplePoint>();
            
            groundTerrainFilter.useLayerMask = true;
            groundTerrainFilter.layerMask = Layers.Terrain;
            groundHits = new List<RaycastHit2D>();
        }

        private void OnEnable()
        {
            grappleSensor.OnSensedObject += OnGrapplePointSensed;
            grappleSensor.OnUnsensedObject += OnGrapplePointUnsensed;
        }

        private void OnDisable()
        {
            grappleSensor.OnSensedObject -= OnGrapplePointSensed;
            grappleSensor.OnUnsensedObject -= OnGrapplePointUnsensed;
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            if (nearestGrapplePoint != null)
            {
                Vector2 grappleDirection = transform.position.DirectionTo(nearestGrapplePoint.Position);
                Gizmos.DrawRay(transform.position, grappleDirection);
            }
        }
#endif

        public IEnumerator GrappleToNearest()
        {
            if (nearestGrapplePoint == null) yield break;

            while (!nearestGrapplePoint.Position.AreClose(ParentTransform.position, grappleTolerance))
            {
                Vector2 grappleDirection = transform.position.DirectionTo(nearestGrapplePoint.Position);
                if (!IsGrapplePathObstructed(grappleDirection, grappleDirection.magnitude))
                    ParentTransform.Translate(grappleDirection.normalized * (grappleSpeed * Time.deltaTime));


                yield return null;
            }
        }

        private void OnGrapplePointSensed(GrapplePoint grapplePoint)
        {
            if (nearestGrapplePoint == null) nearestGrapplePoint = grapplePoint;
            else
            {
                if (transform.position.DistanceTo(grapplePoint.Position) <
                    transform.position.DistanceTo(nearestGrapplePoint.Position))
                    nearestGrapplePoint = grapplePoint;
            }
        }

        private void OnGrapplePointUnsensed(GrapplePoint grapplePoint)
        {
            if (grapplePoint == nearestGrapplePoint)
                nearestGrapplePoint = null;
        }

        private bool IsGrapplePathObstructed(Vector2 direction, float distance)
        {
            Physics2D.Raycast(transform.position, direction, groundTerrainFilter, groundHits, distance);
            return groundHits.Count > 0;
        }
    }
}