﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    // Louis RD
    public class TemperatureBar : MonoBehaviour
    {
        [SerializeField] private Color colorHot = Color.red;
        [SerializeField] private Color colorCold = Color.cyan;
        
        private GameObject temperatureBarFill;
        private SpriteRenderer temperatureBarFillSpriteRenderer;

        private void Start()
        {
            temperatureBarFill = transform.Find(GameObjects.TempratureBarFill).gameObject;
            temperatureBarFillSpriteRenderer = temperatureBarFill.GetComponentInChildren<SpriteRenderer>();
        }

        public void OnTemperatureModified(TemperatureStats playerTemperatureStats)
        {
            SetHorizontalScale(playerTemperatureStats.Temperature);
        }

        public void OnPlayerTemperatureVariationStateChanged(TempState tempState)
        {
            SetTemperatureBarColor(tempState);
        }

        private void SetHorizontalScale(float newHorizontalScale)
        {
            if (newHorizontalScale >= 0 && newHorizontalScale <= 100)
                temperatureBarFill.transform.localScale = new Vector3(newHorizontalScale, 1f);
        }

        private void SetTemperatureBarColor(TempState tempState)
        {
            switch (tempState)
            {
                case TempState.Hot:
                    temperatureBarFillSpriteRenderer.color = colorHot;
                    break;
                case TempState.Frozen:
                    temperatureBarFillSpriteRenderer.color = colorCold;
                    break;
            }
        }
    }
}