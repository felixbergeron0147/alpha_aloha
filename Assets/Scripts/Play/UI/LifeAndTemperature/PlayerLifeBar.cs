﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Louis RD
    public class PlayerLifeBar : MonoBehaviour
    {
        private GameObject lifeBarFill;

        private void Start()
        {
            lifeBarFill = transform.Find(GameObjects.LifeBarFill).gameObject;
        }

        public void OnPlayerLifeModified(Player player)
        {
            SetHorizontalScale(player.GetLifePoints());
        }

        private void SetHorizontalScale(float newHorizontalScale)
        {
            var clampedLifeBarLength = Mathf.Clamp(newHorizontalScale, 0f, 1f);
            lifeBarFill.transform.localScale = new Vector2(clampedLifeBarLength, 1f);
        }
    }
}