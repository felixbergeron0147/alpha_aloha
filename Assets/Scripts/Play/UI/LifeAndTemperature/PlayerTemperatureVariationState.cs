﻿using UnityEngine;

namespace Game
{
    // Louis RD
    public class PlayerTemperatureVariationState : MonoBehaviour
    {
        [SerializeField] private Color colorHot = Color.red;
        [SerializeField] private Color colorCold = Color.cyan;
        
        private SpriteRenderer playerTemperatureVariationStateRenderer;

        private void Awake()
        {
            playerTemperatureVariationStateRenderer = gameObject.GetComponent<SpriteRenderer>();
        }

        public void OnPlayerTemperatureVariationStateChanged(TempState tempState)
        {
            SetPlayerTemperatureVariationStateIndicatorColor(tempState);
        }

        private void SetPlayerTemperatureVariationStateIndicatorColor(TempState tempState)
        {
            switch (tempState)
            {
                case TempState.Hot:
                    playerTemperatureVariationStateRenderer.color = colorHot;
                    break;
                case TempState.Frozen:
                    playerTemperatureVariationStateRenderer.color = colorCold;
                    break;
            }
        }
    }
}