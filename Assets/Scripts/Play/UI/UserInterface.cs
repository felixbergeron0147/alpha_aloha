﻿using Harmony;
using UnityEngine;

namespace Game
{
    // LouisRD
    [Findable(Tags.UICanvas)]
    public class UserInterface : MonoBehaviour
    {
        private Canvas canvas;
        private PlayerLifeBar playerLifeBar;
        private TemperatureBar temperatureBar;
        private PlayerTemperatureVariationState playerTemperatureVariationState;
        private ActionCooldownIcon[] artefactCooldowns;

        private void Awake()
        {
            canvas = gameObject.GetComponent<Canvas>();
            canvas.worldCamera = Camera.main;
            
            playerLifeBar = GetComponentInChildren<PlayerLifeBar>();
            temperatureBar = GetComponentInChildren<TemperatureBar>();
            playerTemperatureVariationState = GetComponentInChildren<PlayerTemperatureVariationState>();
            artefactCooldowns = GetComponentsInChildren<ActionCooldownIcon>();
        }

        public void OnPlayerLifeModified(Player player)
        {
            playerLifeBar.OnPlayerLifeModified(player);
        }

        public void OnTemperatureModified(TemperatureStats playerTemperatureStats)
        {
            temperatureBar.OnTemperatureModified(playerTemperatureStats);
        }

        public void OnPlayerTemperatureVariationStateChanged(TempState tempState)
        {
            playerTemperatureVariationState.OnPlayerTemperatureVariationStateChanged(tempState);
            temperatureBar.OnPlayerTemperatureVariationStateChanged(tempState);
        }

        public void DisplayArtefactCoolDown()
        {
            foreach (var artefactCooldown in artefactCooldowns)
            {
                artefactCooldown.DisplayArtefactCooldown();
            }
        }
    }
}