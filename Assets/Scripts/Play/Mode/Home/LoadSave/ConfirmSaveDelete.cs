﻿using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    // Author: David Dorion && LouisRD
    public class ConfirmSaveDelete : MonoBehaviour
    {
        private TMP_Text confirmText;
        private Button confirmButton;
        private Button cancelButton;
        private string baseText;
        private LoadSaveMenu loadSaveMenu;

        public Button ConfirmButton => confirmButton;

        public SaveSlot SaveSlot
        {
            get;
            private set;
        }

        private void Awake()
        {
            SaveSlot = SaveSlot.None;
            confirmText = GetComponentInChildren<TMP_Text>();
            baseText = confirmText.text;
            loadSaveMenu = GetComponentInParent<LoadSaveMenu>();
            
            var buttons = GetComponentsInChildren<Button>();
            confirmButton = buttons.WithName(GameObjects.ConfirmButton);
            cancelButton = buttons.WithName(GameObjects.CancelButton);
        }

        private void OnEnable()
        {
            cancelButton.onClick.AddListener(Cancel);
            cancelButton.Select();
        }

        private void OnDisable()
        {
            cancelButton.onClick.RemoveListener(Cancel);
        }

        public void SetSaveSlot(SaveSlot saveSlot)
        {
            SaveSlot = saveSlot;
            confirmText.text = baseText.Format((int)saveSlot);
        }

        private void Cancel()
        {
            gameObject.SetActive(false);
            loadSaveMenu.BackButton.Select();
        }
    }
}