using System.Collections;
using Harmony;
using TMPro;
using UnityEngine;

namespace Game
{
    // Author : William L
    [Findable(Tags.GameController)]
    public class GameController : MonoBehaviour
    {
        [SerializeField] private GameObject achievementMessageBox;
        [SerializeField] private string baseAchievementMessage = "Vous avez débloqué le succès: ";
        
        private OnAchievementUnlockedEventChannel onAchievementUnlockedEventChannel;
        private OnPlayerLifeModifiedEventChannel onPlayerLifeModifiedEventChannel;
        private OnTemperatureModifiedEventChannel onTemperatureModifiedEventChannel;
        private OnPlayerTempStateChangedEventChannel onPlayerTempStateChangedEventChannel;

        private AchievementTitle achievementTitle;
        private UserInterface userInterface;
        
        private void Awake()
        {
            onAchievementUnlockedEventChannel = Finder.OnAchievementUnlockedEventChannel;
            onTemperatureModifiedEventChannel = Finder.OnTemperatureModifiedEventChannel;
            onPlayerLifeModifiedEventChannel = Finder.OnPlayerLifeModifiedEventChannel;
            onPlayerTempStateChangedEventChannel = Finder.OnPlayerTempStateChangedEventChannel;

            achievementTitle = Finder.AchievementTitle;
            userInterface = Finder.UserInterface;

            userInterface.gameObject.SetActive(true);
        }

        private void OnEnable()
        {
            onAchievementUnlockedEventChannel.OnAchivementUnlocked += OnTrophyUnlocked;
            onTemperatureModifiedEventChannel.OnTemperatureModified += OnTemperatureModified;
            onPlayerLifeModifiedEventChannel.OnPlayerLifeModified += OnPlayerLifeModified;
            onPlayerTempStateChangedEventChannel.OnPlayerTempStateChanged += OnPlayerTempStateChanged;
        }
        private void OnDisable()
        {
            onAchievementUnlockedEventChannel.OnAchivementUnlocked -= OnTrophyUnlocked;
            onTemperatureModifiedEventChannel.OnTemperatureModified -= OnTemperatureModified;
            onPlayerLifeModifiedEventChannel.OnPlayerLifeModified -= OnPlayerLifeModified;
            onPlayerTempStateChangedEventChannel.OnPlayerTempStateChanged -= OnPlayerTempStateChanged;
        }
        
        private void OnTrophyUnlocked(AchievementType achievement)
        {
            StartCoroutine(NoticeUnlockedTrophy());

            IEnumerator NoticeUnlockedTrophy()
            {
                var trophyTextMesh = achievementMessageBox.GetComponent<TextMeshProUGUI>();
                trophyTextMesh.text = baseAchievementMessage + achievementTitle.Title(achievement);
                trophyTextMesh.enabled = true;
                yield return new WaitForSeconds(5);
                trophyTextMesh.enabled = false;
            }
        }

        private void OnTemperatureModified(TemperatureStats playerTemperatureStats)
        {
            userInterface.OnTemperatureModified(playerTemperatureStats);
        }

        private void OnPlayerLifeModified(Player player)
        {
            userInterface.OnPlayerLifeModified(player);
        }

        private void OnPlayerTempStateChanged(TempState tempState)
        {
            userInterface.OnPlayerTemperatureVariationStateChanged(tempState);
        }
    }
}