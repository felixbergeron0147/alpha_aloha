using UnityEngine;

namespace Game
{
    // Author: David D, Félix B
    [RequireComponent(typeof(TemperatureStats))]
    public abstract class ObjectTemperature : MonoBehaviour, ITemperature
    {
        [SerializeField] [Range(0f, 1f)] private float naturalTemperature = 0.5f;
        [SerializeField] [Tooltip("Puissance/Vitesse avec laquelle l'entité tend à retourner à sa température de base.")]
        [Range(0f, 1f)] private float normalizingFactor = 0.25f;

        protected TemperatureStats temperatureStats;

        public TemperatureStats TemperatureStats => temperatureStats;
        protected Color TempColor => temperatureStats.TempColor;
        private float ObjectTemperatureVariationFactor
        {
            get
            {
                if (Mathf.Approximately(temperatureStats.Temperature, naturalTemperature))
                    return 0f;
                if (temperatureStats.Temperature < naturalTemperature)
                    return normalizingFactor;
                if (temperatureStats.Temperature > naturalTemperature)
                    return -normalizingFactor;

                return 0f;
            }
        }

        protected virtual void Awake()
        {
            temperatureStats = GetComponent<TemperatureStats>();
        }

        protected virtual void Update()
        {
            temperatureStats.ObjectInfluence = ObjectTemperatureVariationFactor;
        }
    }
}