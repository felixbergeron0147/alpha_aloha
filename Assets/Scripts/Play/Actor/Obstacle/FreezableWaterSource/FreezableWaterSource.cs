using Harmony;
using UnityEngine;

namespace Game
{
    // Author: Félix B, David D
    public class FreezableWaterSource : ObjectTemperature
    {
        private GameObject frozenStructureGameObject;
        private GameObject liquidStructureGameObject;
        private SpriteRenderer sprite;
        
        private new void Awake()
        {
            base.Awake();
            
            frozenStructureGameObject = transform.Find(GameObjects.FrozenStateStructure).gameObject;
            liquidStructureGameObject = GetComponentInChildren<LiquidStateStructure>().gameObject;
            
            ChangeStates(temperatureStats);
        }

        private void OnEnable()
        {
            temperatureStats.OnChangeTempState += ChangeStates;
        }

        private void OnDisable()
        {
            temperatureStats.OnChangeTempState -= ChangeStates;
        }

        private void ChangeStates(TemperatureStats temperatureStats)
        {
            switch (temperatureStats.TemperatureState)
            {
                case TempState.Frozen:
                    liquidStructureGameObject.SetActive(false);
                    frozenStructureGameObject.SetActive(true);
                    break;
                case TempState.Hot:
                    frozenStructureGameObject.SetActive(false);
                    liquidStructureGameObject.SetActive(true);
                    break;
            }
        }
    }
}