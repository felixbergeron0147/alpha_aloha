using UnityEngine;

namespace Game
{
    // Author : Félix B
    public class FrozenStateStructure : MonoBehaviour
    {
        private SpriteRenderer sprite;
        private BoxCollider2D collider;

        private void Awake()
        {
            sprite = GetComponent<SpriteRenderer>();
            collider = GetComponent<BoxCollider2D>();
        }

        private void OnEnable()
        {
            collider.size = sprite.size;
        }
    }
}