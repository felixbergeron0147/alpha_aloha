using System.Collections;
using UnityEngine;

namespace Game
{
    // Author : Félix B
    public class LiquidStateStructure : MonoBehaviour
    {
        [SerializeField] [Range(0f, 1f)] private float damageAmount = 1f;
        [SerializeField] private DamageType damageType = DamageType.Water;
        [SerializeField] [Min(0f)] private float animationSpeed = 0.5f;
        
        private ISensor<Player> playerSensor;
        private ISensor<EssentialRespawner> essentialRespawnerSensor;
        private SpriteRenderer sprite;
        
        private void Awake()
        {
            playerSensor = GetComponentInChildren<TriggerSensor2D>().For<Player>();
            essentialRespawnerSensor = GetComponentInChildren<TriggerSensor2D>().For<EssentialRespawner>();
            sprite = GetComponentInParent<SpriteRenderer>();
        }

        private void OnEnable()
        {
            playerSensor.OnSensedObject += OnPlayerSensed;
            essentialRespawnerSensor.OnSensedObject += OnEssentialRespawnerSensed;
            essentialRespawnerSensor.OnUnsensedObject += OnUnsensed;
            
            StartCoroutine(LiquidAnimationRoutine());
        }
        
        private void OnUnsensed(EssentialRespawner essentialRespawner) {/* Needed because of a bug */}

        private void OnDisable()
        {
            playerSensor.OnSensedObject -= OnPlayerSensed;
            essentialRespawnerSensor.OnSensedObject -= OnEssentialRespawnerSensed;
            essentialRespawnerSensor.OnUnsensedObject -= OnUnsensed;
        }

        private IEnumerator LiquidAnimationRoutine()
        {
            while (isActiveAndEnabled)
            {
                sprite.flipX = !sprite.flipX;
                yield return new WaitForSeconds(animationSpeed);
            }
        }

        private void OnPlayerSensed(Player player)
        {
            player.Hurt(damageAmount);
        }
        
        private void OnEssentialRespawnerSensed(EssentialRespawner essentialRespawner)
        {
            essentialRespawner.Respawn(damageType);
        }
    }
}