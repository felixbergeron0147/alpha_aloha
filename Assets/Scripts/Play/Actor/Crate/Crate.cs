﻿using UnityEngine;

namespace Game
{
    // Author: Félix B
    public class Crate : MonoBehaviour, IEntity, IEssential
    {
        public Vector3 Position { get; }

        private Rigidbody2D body;

        private Vector2 startPositon;
        
        private void Start()
        {
            startPositon = transform.position;
        }

        public void Reset()
        {
            transform.position = startPositon;
        }
    }
}