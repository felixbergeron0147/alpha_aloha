﻿using System.Collections;
using UnityEngine;

namespace Game
{
    // Author: David D
    public class EnemyIceBlockController : MonoBehaviour
    {
        [SerializeField] private float freezeTime = 4f;
        
        private BoxCollider2D boxCollider2D;
        private Enemy enemy;
        private SpriteRenderer spriteRenderer;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            boxCollider2D = GetComponent<BoxCollider2D>();
            enemy = GetComponentInParent<Enemy>();
        }

        public void EnableFreeze()
        {
            spriteRenderer.enabled = true;
            boxCollider2D.enabled = true;
            enemy.enabled = false;
            
            StartCoroutine(UnFreezeCoroutine());
        }
        
        private void DisableFreeze()
        {
            spriteRenderer.enabled = false;
            boxCollider2D.enabled = false;
            enemy.enabled = true;
        }
        
        private IEnumerator UnFreezeCoroutine()
        {
            yield return new WaitForSeconds(freezeTime);
            DisableFreeze();
        }
    }
}