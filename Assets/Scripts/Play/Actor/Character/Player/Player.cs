using System.Collections;
using Harmony;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game
{
    // Author : Tout le monde
    [Findable(Tags.Player)]
    public class Player : Character, IHurtable, IHurtableTemperature, IArtefactCollector, ITemperature
    {
        [Header("Other")] 
        [SerializeField] private bool isInvincible;
        [SerializeField] private bool isSpecialFalling = false;

        [Header("PlayerTemperatureStats")]
        [SerializeField] [Tooltip("Montant de dégat prit par seconde")]
        private float temperatureDPS = 0.05f;
        [SerializeField] private float freezingTime = 2f;
        [SerializeField] private float freezeEffectDelay = 4f;
        [SerializeField] [Range(-1f, 0f)] private float playerTemperatureColdRate = -0.5f;
        [SerializeField] [Range(0f, 1f)] private float playerTemperatureHotRate = 0.5f;

        [Header("Movement Settings")] 
        [SerializeField] private bool doubleJumpUnlocked;
        [SerializeField] private bool canInfiniteJump;

        private bool hasTouchedGroundSinceLastJump;
        private bool canDoubleJump;
        private bool hasFrozeInputs;
        private bool startScene;
        private bool slideToggle;

        private InputAction dashInput;
        private InputActions.GameActions gameInputs;
        private InputAction grappleInput;
        private InputAction jumpInputs;
        private InputAction freezeInput;
        private InputAction switchTempStateInputs;
        
        private OnPlayerDeathEventChannel onPlayerDeathEventChannel;
        private OnPlayerLifeModifiedEventChannel onPlayerLifeModifiedEventChannel;
        private OnTemperatureModifiedEventChannel onTemperatureModifiedEventChannel;
        private OnPlayerTempStateChangedEventChannel onPlayerTempStateChangedEventChannel;
        private OnActionActivatedChannel onActionActivatedChannel;

        private Rigidbody2D rigidbody2D;
        private CapsuleCollider2D collider;
        private FreezeTriggerZone freezeTriggerZone;
        private PlayerEffectsAnimator playerEffects;
        private PlayerMotionAnimator playerMotions;
        private GameMemory gameMemory;
        private FullLevelCamera fullLevelCamera;
        private PlayerTemperatureAura aura;
        private Grapple grapple;
        private DashSensor dashSensor;
        private UserInterface ui;

        private bool SlideToggle
        {
            set
            {
                slideToggle = value;
                Mover.SlideToggle = value;
            }
        }

        public bool IsInHub { get; set; }
        public TempState PlayerTemperatureState { get; private set; }
        public TemperatureStats TemperatureStats { get; private set; }
        private InputAction MoveInputs { get; set; }
        public FreezeTriggerZone FreezeTriggerZone => freezeTriggerZone;
        private float PlayerTemperatureVariationFactor
        {
            get
            {
                switch (PlayerTemperatureState)
                {
                    case TempState.Frozen:
                        return playerTemperatureColdRate;
                    case TempState.Hot:
                        return playerTemperatureHotRate;
                }

                return 0f;
            }
        }

        private new void Awake()
        {
            base.Awake();
            
            gameObject.layer = Layers.Player;

            MoveInputs = Finder.Inputs.Actions.Game.Move;
            jumpInputs = Finder.Inputs.Actions.Game.Jump;
            switchTempStateInputs = Finder.Inputs.Actions.Game.SwitchTemperatureState;
            gameInputs = Finder.Inputs.Actions.Game;
            grappleInput = Finder.Inputs.Actions.Game.Grapple;
            freezeInput = Finder.Inputs.Actions.Game.Freeze;
            dashInput = Finder.Inputs.Actions.Game.Dash;

            rigidbody2D = GetComponent<Rigidbody2D>();
            collider = GetComponent<CapsuleCollider2D>();
            aura = GetComponentInChildren<PlayerTemperatureAura>();
            freezeTriggerZone = GetComponentInChildren<FreezeTriggerZone>();
            TemperatureStats = GetComponent<TemperatureStats>();
            fullLevelCamera = gameObject.Parent().GetComponentInChildren<FullLevelCamera>();
            grapple = GetComponentInChildren<Grapple>();
            
            ui = Finder.UserInterface;
            gameMemory = Finder.GameMemory;
            
            onPlayerDeathEventChannel = Finder.OnPlayerDeathEventChannel;
            onPlayerLifeModifiedEventChannel = Finder.OnPlayerLifeModifiedEventChannel;
            onTemperatureModifiedEventChannel = Finder.OnTemperatureModifiedEventChannel;
            onTemperatureModifiedEventChannel.Publish(TemperatureStats);
            onPlayerTempStateChangedEventChannel = Finder.OnPlayerTempStateChangedEventChannel;
            onActionActivatedChannel = Finder.OnActionActivatedChannel;

            GameObject dashGameObject = GameObject.Find(GameObjects.DashSensor);
            dashSensor = dashGameObject.GetComponent<DashSensor>();

            GameObject playerEffectsGo = GameObject.Find(GameObjects.PlayerEffect);
            playerEffects = playerEffectsGo.GetComponent<PlayerEffectsAnimator>();

            GameObject playerMotionsGo = GameObject.Find(GameObjects.PlayerMotionAnimator);
            playerMotions = playerMotionsGo.GetComponent<PlayerMotionAnimator>();

            SwitchTempState();
            
            PlayerTemperatureState = gameMemory.LastPlayerState;
            
            slideToggle = false;
        }

        private new void OnEnable()
        {
            base.OnEnable();
            
            playerMotions.SwitchColor(PlayerTemperatureState);
            startScene = false;
            onPlayerLifeModifiedEventChannel.Publish(this);
            
            TemperatureStats.OnColdThreshold += FreezePlayer;
            TemperatureStats.OnHeatThreshold += BurnPlayer;
        }

        private void Start()
        {
            onPlayerTempStateChangedEventChannel.Publish(PlayerTemperatureState);

            IEnumerator Routine()
            {
                EnableTemperatureVariation(false);
                yield return new WaitForSeconds(2);
                EnableControls();
            }

            StartCoroutine(Routine());
            
            // Tout les méthodes qui sont en lien avec cet appel son necasaire,
            // car Finder.player ne fonctionne pas dans le Start des ArtefactCooldown
            ui.DisplayArtefactCoolDown();
        }

        private void Update()
        {
            UpdatePlayerMotionState();
            
            onPlayerLifeModifiedEventChannel.Publish(this);
            onTemperatureModifiedEventChannel.Publish(TemperatureStats);

            TemperatureStats.ObjectInfluence = PlayerTemperatureVariationFactor;

            if (!startScene)
            {
                if (MoveInputs.triggered)
                {
                    fullLevelCamera.StartPlayerCamera();
                    if (!IsInHub)
                        EnableTemperatureVariation(true);
                    startScene = true;
                }
            }

            if (terrainSensor.SensedObjects.Count > 0)
            {
                Mover.TouchIce = terrainSensor.SensedObjects[0].TemperatureStats.TemperatureState == TempState.Frozen;
            }

            VerifiyUpcomingDashCollision();

            #region Controls

            Mover.IsGrounded = IsGrounded;

            FacingDirection = MoveInputs.ReadValue<Vector2>();

            Mover.Move(MoveInputs.ReadValue<Vector2>());

            if (switchTempStateInputs.triggered)
            {
                SwitchTempState();
                aura.OnPlayerStateChange();
                playerMotions.SwitchColor(PlayerTemperatureState);
            }

            if (!hasTouchedGroundSinceLastJump && IsGrounded && rigidbody2D.velocity.y <= 0)
                hasTouchedGroundSinceLastJump = IsGrounded;

            if (!isSpecialFalling)
            {
                if (jumpInputs.triggered)
                {
                    if (canInfiniteJump)
                    {
                        Mover.Jump();
                        playerEffects.FireJumpEffect();
                    }
                    else if (hasTouchedGroundSinceLastJump)
                    {
                        Mover.Jump();
                        playerEffects.FireJumpEffect();
                        canDoubleJump = true;
                        hasTouchedGroundSinceLastJump = false;
                    }
                    else if (doubleJumpUnlocked && canDoubleJump)
                    {
                        Mover.DoubleJump();
                        playerEffects.FireJumpEffect();
                        canDoubleJump = false;
                    }
                }

                if (gameInputs.Slide.triggered)
                    SlideToggle = !slideToggle;

                if (dashInput.triggered)
                {
                    if (Mover.Dash(FacingDirection))
                    {
                        onActionActivatedChannel.Publish(ActionsWithCooldown.Dash);
                        playerEffects.FireDashEffect();
                        isSpecialFalling = true;
                    }
                }

                if (gameMemory.HasCollectedArtefact(ArtefactType.Grapple) && grappleInput.triggered)
                    StartCoroutine(grapple.GrappleToNearest());


                if (gameMemory.HasCollectedArtefact(ArtefactType.Freeze) && freezeInput.triggered &&
                    PlayerTemperatureState == TempState.Frozen)
                {
                    if (freezeTriggerZone.Freeze())
                    {
                        onActionActivatedChannel.Publish(ActionsWithCooldown.FreezeEnemy);
                        playerEffects.FireFreezeEffect();
                    }
                }
            }
            else if (IsGrounded)
            {
                isSpecialFalling = false;
            }

            #endregion

#if UNITY_EDITOR
            if (gameInputs.ActivateInvincibility.triggered)
                isInvincible = !isInvincible;
            if (gameInputs.ActivateInfiniteJump.triggered)
                canInfiniteJump = !canInfiniteJump;
#endif

            UpdateOrientationDependentComponente();
        }

        private void OnDisable()
        {
            DisableControls();
            gameMemory.LastPlayerState = PlayerTemperatureState;

            TemperatureStats.OnColdThreshold -= FreezePlayer;
            TemperatureStats.OnHeatThreshold -= BurnPlayer;
        }

        // Author: David D
        public void CollectArtefact(ArtefactType artefactType)
        {
            switch (artefactType)
            {
                case ArtefactType.Geyser:
                {
                    gameMemory.AddArtefactToCollectedList(ArtefactType.Geyser);
                    break;
                }
                case ArtefactType.Grapple:
                {
                    grappleInput.Enable();
                    gameMemory.AddArtefactToCollectedList(ArtefactType.Grapple);
                    break;
                }
                case ArtefactType.Freeze:
                {
                    freezeInput.Enable();
                    gameMemory.AddArtefactToCollectedList(ArtefactType.Freeze);
                    break;
                }
                case ArtefactType.DashBreak:
                {
                    gameMemory.AddArtefactToCollectedList(ArtefactType.DashBreak);
                    break;
                }
                case ArtefactType.DoubleJump:
                {
                    doubleJumpUnlocked = true;
                    gameMemory.AddArtefactToCollectedList(ArtefactType.DoubleJump);
                    break;
                }
            }
        }

        //Author: Félix B
        public void Hurt(float damage)
        {
            if (isInvincible) return;

            Vitals.TakeDamage(damage);
            onPlayerLifeModifiedEventChannel.Publish(this);

            if (Vitals.IsDead)
            {
                isInvincible = true; //NE PAS EFFACER POUR L'INSTANT EMPÊCHE LE NIVEAU DE CHARGER PLUS D'UNE FOIS.
                rigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;
                onPlayerDeathEventChannel.Publish();
                gameMemory.CleanData();
            }
        }

        public void DisableControls()
        {
            gameInputs.Disable();
        }

        // Author: David D
        public void EnableControls()
        {
            gameInputs.Enable();

            if (gameMemory.HasCollectedArtefact(ArtefactType.Grapple)) grappleInput.Enable();

            if (gameMemory.HasCollectedArtefact(ArtefactType.Freeze)) freezeInput.Enable();

            if (gameMemory.HasCollectedArtefact(ArtefactType.DoubleJump)) doubleJumpUnlocked = true;
        }

        // Author: Félix B
        private void UpdateOrientationDependentComponente()
        {
            if (FacingDirection == Vector2.left)
            {
                FlipX(true);
            }
            else if (FacingDirection == Vector2.right)
            {
                FlipX(false);
            }
        }

        public void SetPlayerIsInvincible(bool isInvincible)
        {
            this.isInvincible = isInvincible;
        }

        public float GetLifePoints()
        {
            return Vitals.LifePoints;
        }

        // Author: Félix B
        private void VerifiyUpcomingDashCollision()
        {
            if (Mover.IsDashing)
            { 
                if (dashSensor.DashDestroyableSensor2D.SensedObjects.Count > 0)
                {
                    foreach (var destroyable in dashSensor.DashDestroyableSensor2D.SensedObjects)
                    {
                        float timeBeforeCollision = GetTimeBeforeCollision(destroyable);

                        if (!destroyable.DoorBreakingArtefactNeeded)
                        {
                            StartCoroutine(destroyable.DestructionCountDown(timeBeforeCollision));
                        }
                        else if (gameMemory.HasCollectedArtefact(ArtefactType.DashBreak))
                        {
                            StartCoroutine(destroyable.DestructionCountDown(timeBeforeCollision));
                        }
                    }
                }
            }
        }

        // Author: Félix B
        private float GetTimeBeforeCollision(IDashDestroyable destroyable)
        {
            float distance = collider.Distance(destroyable.Collider).distance;
            float timeBeforeCollision = distance / rigidbody2D.velocity.magnitude;
            return timeBeforeCollision;
        }
        
        // Author: Félix B
        private void UpdatePlayerMotionState()
        {
            PlayerMotionState newState = PlayerMotionState.Idle;
                
            if (rigidbody2D.velocity.x != 0)
                newState = PlayerMotionState.Running;
            
            if (Vitals.IsDead)
                newState = PlayerMotionState.Dying;
            else if (Mover.IsDashing)
                newState = PlayerMotionState.Dashing;
            else if(rigidbody2D.velocity.y < 0 && !IsGrounded)
                newState = PlayerMotionState.Falling;
            else if(rigidbody2D.velocity.y > 0 && !IsGrounded)
                newState = PlayerMotionState.Jumping;
            
            playerMotions.MotionState = newState;
        }

        #region Temperature

        public void HurtTemperatureFromEnemy(TempState enemyState)
        {
#if UNITY_EDITOR
            Debug.Assert(TemperatureStats != null, "playerTemperature is null on HurtTemperature");
#endif
            switch (enemyState)
            {
                case TempState.Frozen:
                    TemperatureStats.SetTemperatureToMin();
                    break;
                case TempState.Hot:
                    TemperatureStats.SetTemperatureToMax();
                    break;
            }
        }

        private void EnableTemperatureVariation(bool enableTemperatureVariation)
        {
#if UNITY_EDITOR
            Debug.Assert(TemperatureStats != null, "playerTemperature is null on EnableTemperatureVariation");
            Debug.Assert(onTemperatureModifiedEventChannel != null,
                "onTemperatureModifiedEventChannel is null on ForcePublishOnTemperatureModifiedEventChannel");
#endif
            onTemperatureModifiedEventChannel.Publish(TemperatureStats);
            TemperatureStats.enabled = enableTemperatureVariation;
        }

        // Author: David D, Félix B
        private void BurnPlayer(TemperatureStats temperatureStats)
        {
            Hurt(temperatureDPS * Time.deltaTime);

            StartCoroutine(BurnPlayerRoutine());
        }

        // Author: David D, Félix B
        private void FreezePlayer(TemperatureStats temperatureStats)
        {
            Hurt(temperatureDPS * Time.deltaTime);

            if (!hasFrozeInputs)
                StartCoroutine(FreezePlayerRoutine());
        }

        // Author: David D, Félix B
        private IEnumerator FreezePlayerRoutine()
        {
            hasFrozeInputs = true;
            DisableControls();
            yield return new WaitForSeconds(freezingTime);
            EnableControls();
            yield return new WaitForSeconds(freezeEffectDelay);
            hasFrozeInputs = false;
        }

        // Author: David D, Félix B
        private IEnumerator BurnPlayerRoutine()
        {
            while (TemperatureStats.HasReachMaxTemperature)
            {
                Mover.Move(FacingDirection);

                yield return null;
            }
        }

        // Author: David D, Félix B
        private void SwitchTempState()
        {
            PlayerTemperatureState = PlayerTemperatureState.Next();
            onPlayerTempStateChangedEventChannel.Publish(PlayerTemperatureState);
        }

        #endregion
    }
}