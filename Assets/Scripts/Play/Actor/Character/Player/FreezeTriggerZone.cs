﻿using System.Collections;
using UnityEngine;

namespace Game
{
    // Author: David D
    public class FreezeTriggerZone : MonoBehaviour
    {
        [SerializeField] [Min(0f)] private float coolDown = 5f;
        
        private ISensor<Enemy> enemySensor;
        private bool isFreezing;

        public float CoolDown => coolDown;

        private void Awake()
        {
            enemySensor = GetComponent<TriggerSensor2D>().For<Enemy>();
            isFreezing = false;
        }

        public bool Freeze()
        {
            if (isFreezing) return false;
            
            StartCoroutine(FreezeRoutine());
            return true;
        }

        private IEnumerator FreezeRoutine()
        {
            isFreezing = true;
            foreach (var enemy in enemySensor.SensedObjects) enemy.Freeze();
            yield return new WaitForSeconds(coolDown);
            isFreezing = false;
        }
    }
}