using System.Collections;
using UnityEngine;

namespace Game
{
    // Author : Félix B
    public class ConcreteDashDestroyable : MonoBehaviour, IDashDestroyable
    {
        [SerializeField] private bool doorBreakingArtefactNeeded = true;

        private Collider2D collider;

        public Collider2D Collider
        {
            get => collider;
        }

        private void Awake()
        {
            collider = GetComponentInChildren<Collider2D>();
        }

        public bool DoorBreakingArtefactNeeded
        {
            get => doorBreakingArtefactNeeded;
        }

        public IEnumerator DestructionCountDown(float timeBeforeDestruction)
        {
            collider.enabled = false;
            
            yield return new WaitForSeconds(timeBeforeDestruction);
            gameObject.SetActive(false);
        }
    }
}