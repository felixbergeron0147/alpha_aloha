﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Game
{
    // Author: David Dorion, Felix B
    public class TemporaryPlatform : MonoBehaviour
    {
        [Header("Temporary Platform")] 
        [SerializeField] [Min(0f)] private float respawnDelay = 2f;
        [SerializeField] [Min(0f)] private float disappearDelay = 2f;
        [SerializeField] [Min(0f)] private float fadeInDuration = 0.2f;
        [SerializeField] [Min(0f)] private float fadeOutDuration = 0.2f;
        [SerializeField] [Min(0f)] private float defaultShakeForce = 0.2f;
        [SerializeField] [Min(0f)] private float disappearShakeForce = 0.4f;
        [SerializeField] [Min(0f)] private float shakeSpeed = 30f;

        private BoxCollider2D boxCollider2D;
        private SpriteRenderer spriteRenderer;
        private Vector2 startPosition;
        private bool isDisappearing;

        private void Awake()
        {
            startPosition = transform.position;
            boxCollider2D = GetComponent<BoxCollider2D>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            isDisappearing = false;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (isDisappearing) return;
            
            StartCoroutine(DisappearRoutine());
        }

        private void Update()
        {
            var shakeForce = isDisappearing ? disappearShakeForce : defaultShakeForce;

            Vector2 newPos = new Vector2(startPosition.x + Mathf.Sin(Time.time * shakeSpeed) * shakeForce,
                startPosition.y + (Mathf.Sin(Time.time * shakeSpeed) * shakeForce));
            transform.position = newPos;
        }
        
        private IEnumerator DisappearRoutine()
        {
            //Il commence à trembler fortement au contact.
            isDisappearing = true;
            yield return new WaitForSeconds(disappearDelay);
            //Il disparait.
            boxCollider2D.enabled = false;
            yield return spriteRenderer.DOFade(0, fadeOutDuration).WaitForCompletion();
            //Il se replace et attend de réaparaitre.
            isDisappearing = false;
            transform.position = startPosition;
            yield return new WaitForSeconds(respawnDelay);
            //Il commence à réaparaitre.
            yield return spriteRenderer.DOFade(1, fadeInDuration).WaitForCompletion();
            boxCollider2D.enabled = true;
            //Il est réaparu.
        }
    }
}