﻿using System.Collections;
using UnityEngine;

namespace Game
{
    // Author: David D
    public class SignalButton : MonoBehaviour
    {
        [SerializeField] private Color inactiveColor = Color.red;
        [SerializeField] private Color activeColor = Color.green;
        [SerializeField] [Min(0f)] private float activeTime = 2f;
        
        private InteractSensor interactSensor;
        private SignalSender signalSender;
        private SpriteRenderer spriteRenderer;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.color = inactiveColor;
            interactSensor = GetComponent<InteractSensor>();
            signalSender = GetComponent<SignalSender>();
        }

        private void OnEnable()
        {
            interactSensor.OnInteract += UpdateState;
        }

        private void OnDisable()
        {
            interactSensor.OnInteract -= UpdateState;
        }

        private void UpdateState(InteractSensor interactSensor)
        {
            IEnumerator PressButton()
            {
                signalSender.IsActivated = true;
                ChangeColor();
                yield return new WaitForSeconds(activeTime);
                signalSender.IsActivated = false;
                ChangeColor();
            }
            
            if (!signalSender.RawActivated)
                StartCoroutine(PressButton());
        }

        private void ChangeColor()
        {
            spriteRenderer.color = signalSender.RawActivated ? activeColor : inactiveColor;
        }
    }
}