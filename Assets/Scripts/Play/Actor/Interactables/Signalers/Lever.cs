﻿using UnityEngine;

namespace Game
{
    // Author: David D
    public class Lever : MonoBehaviour
    {
        private InteractSensor interactSensor;
        private SignalSender signalSender;
        private SpriteRenderer spriteRenderer;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            interactSensor = GetComponent<InteractSensor>();
            signalSender = GetComponent<SignalSender>();
        }

        private void OnEnable()
        {
            interactSensor.OnInteract += OnInteracted;
        }

        private void OnDisable()
        {
            interactSensor.OnInteract -= OnInteracted;
        }

        private void OnInteracted(InteractSensor interactSensor)
        {
            signalSender.ToggleActivation();
            ChangeVisuals();
        }

        private void ChangeVisuals()
        {
            spriteRenderer.flipX = signalSender.RawActivated;
        }
    }
}