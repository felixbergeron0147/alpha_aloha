﻿using System;
using UnityEngine;

namespace Harmony
{
    [AttributeUsage(AttributeTargets.Class)]
    public class FindableAttribute : PropertyAttribute
    {
        public FindableAttribute(string tag)
        {
        }
    }
}