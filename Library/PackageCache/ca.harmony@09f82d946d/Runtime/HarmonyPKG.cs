﻿using System.IO;

namespace Harmony
{
    public static class HarmonyPkg
    {
        public static readonly string PackagePath = Path.GetFullPath("Packages/ca.harmony");
#if UNITY_EDITOR_WIN
        public static readonly string GeneratorPath = Path.Combine(PackagePath, "Binairies/CodeGenerator/code-generator.exe");
#else
        public static readonly string GeneratorPath = Path.Combine(PackagePath, "Binairies/CodeGenerator/code-generator");
#endif
    }
}