﻿using System;
using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Harmony
{
    public class CodeGenerator : EditorWindow
    {
        private String stdOut = "";
        private Vector2 stdOutScroll;
        private String stdErr = "";
        private Vector2 stdErrScroll;

        [MenuItem("Tools/Harmony/Code Generator...", priority = 100)]
        public static void GenerateConstClasses()
        {
            GetWindow<CodeGenerator>(false, "Harmony Code Generator", true).Show();
        }

        private void OnEnable()
        {
            minSize = new Vector2(0, 480);
        }

        private void OnGUI()
        {
            EditorGUILayout.LabelField("Output", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            stdOutScroll = GUILayout.BeginScrollView(stdOutScroll, GUILayout.Height(200));
            GUILayout.TextArea(stdOut, EditorStyles.label, GUILayout.ExpandWidth(true));
            GUILayout.EndScrollView();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.LabelField("Errors", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            stdErrScroll = GUILayout.BeginScrollView(stdErrScroll, GUILayout.Height(200));
            GUILayout.TextArea(stdErr, EditorStyles.label, GUILayout.ExpandWidth(true));
            GUILayout.EndScrollView();
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Generate"))
            {
                GenerateCode();
                RefreshAssets();
            }
        }

        private void OnInspectorUpdate()
        {
            Repaint();
        }

        private void GenerateCode()
        {
            var pathToScript = HarmonyPkg.GeneratorPath;
            var pathToProjectDirectory = Path.GetFullPath(Path.Combine(Application.dataPath, ".."));
            var pathToGeneratedDirectory = Path.Combine(pathToProjectDirectory, "Assets/Generated/Harmony");

            var processStartInfo = new ProcessStartInfo
            {
                FileName = pathToScript,
                Arguments = $"--input-dir \"{pathToProjectDirectory}\" --output-dir \"{pathToGeneratedDirectory}\"",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            var process = Process.Start(processStartInfo);
            stdOut = process.StandardOutput.ReadToEnd();
            stdErr = process.StandardError.ReadToEnd();
        }

        private static void RefreshAssets()
        {
            AssetDatabase.Refresh();
        }
    }
}